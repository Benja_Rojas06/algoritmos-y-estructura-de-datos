#include <iostream>
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

// Constructor clase Atomo.
Atomo::Atomo(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

// Métodos set.
void Atomo::set_nombre(string nombre){
    this->nombre = nombre;
}

void Atomo::set_numero(int numero){
    this->numero = numero;
}

void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}

// Métodos get.
string Atomo::get_nombre(){
    return this->nombre;
}

int Atomo::get_numero(){
    return this->numero;
}

Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}
