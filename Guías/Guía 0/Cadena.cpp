#include <iostream>
#include <list>
#include "Cadena.h"
#include "Aminoacido.h"

using namespace std;

// Constructor clase Cadena.
Cadena::Cadena(string letra){
    this->letra = letra;
}

// Métodos set.
void Cadena::set_letra(string letra){
    this->letra = letra;
}

void Cadena::add_aminoacido(Aminoacido aminoacido){
    // Se agrega cada aminoacido a la lista "aminoacidos".
    this->aminoacidos.push_back(aminoacido);
}

// Métodos get.
string Cadena::get_letra(){
    return this->letra;
}

list<Aminoacido> Cadena::get_aminoacidos(){
    return this->aminoacidos;
}
