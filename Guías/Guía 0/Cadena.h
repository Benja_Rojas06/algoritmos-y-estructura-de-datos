#include <iostream>
#include <list>
#include "Aminoacido.h"

using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena{

    private:
        string letra;
        list<Aminoacido> aminoacidos;

    public:
        // Constructor.
        Cadena(string letra);

        // Métodos.
        void set_letra(string letra);
        void add_aminoacido(Aminoacido aminoacido);
        string get_letra();
        list<Aminoacido> get_aminoacidos();
};
#endif
