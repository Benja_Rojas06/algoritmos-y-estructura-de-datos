# Propuesta solución Guía 0:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste, básicamente, en un programa en el cual se pueden ingresar proteínas y
los datos de estas, tales como ID, nombre, cantidad de cadenas (Con una letra
previamente asignada), cantidad de aminoácidos, átomos y las coordenadas de
estos en una dimensión 3D.


## Funcionamiento e Interacción:

- El programa comienza con el despliegue del menú principal con 3 opciones:
Ingresar una proteína, visualizar las previamente ingresadas (Por el usuario) y
salir. Al presionar "1", se solicitarán todos los datos correspondientes a la
proteína que se desea ingresar y, una vez ingresados, todos los datos se
guardarán en una lista. Al presionar "2" tenemos dos casos, el primero
es aquel en el que 1 o más proteínas han sido ingresadas, por lo cual se
imprimirán en pantalla (Debido a que se accede a la lista de proteínas) y luego
de esto se le dará la opción al usuario de cerrar el programa o de volver al
menú principal; el segundo caso es en el que aún no se ha ingresado ninguna
proteína, por lo cual se le avisará de esto al usuario y volverá a la pantalla
principal. Por último, al presionar "3", se dará paso al cierre del programa.
- Cabe mencionar que si se ingresa cualquier otro número, se indicará al usuario
que está cometiendo un error y se volverá a la pantalla incial.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.


## Puntos a considerar:

- Cada clase posee su archivo .cpp y .h como se solicita en la guía.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./programa".
- Se implementa un menú básico en el cual se indican las acciones que se pueden
realizar.
