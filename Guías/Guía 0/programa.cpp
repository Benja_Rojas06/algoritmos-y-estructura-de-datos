#include <iostream>
#include <list>
#include <stdlib.h>
#include "Proteina.h"
#include "Aminoacido.h"
#include "Cadena.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

int menu(){

    string accion;
    system("clear");

    cout << "Bienvenido a la propuesta de solución Guía 0." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl;
    cout << "1.- Ingresar datos de una proteína." << endl;
    cout << "2.- Revisar datos de proteínas ingresadas." << endl;
    cout << "3.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    getline(cin, accion);

    return stoi(accion);
}

void imprimir_datos_proteina(list<Proteina> proteinas){

    string eleccion;

    system("clear");
    cout << "Las proteínas ingresadas hasta el momento son:" << endl << endl;

    // Impresión de todos los datos correspondientes a la(s) proteína.
    for (Proteina p: proteinas){
        // Imprime datos proteína(s).
        cout << "ID: " << p.get_id() << endl;
        cout << "Nombre: " << p.get_nombre() << endl;

        for (Cadena c: p.get_cadenas()){
            // Imprime datos cadena(s).
            cout << "\nCadena " << c.get_letra() << ":" << endl;

            for (Aminoacido aa: c.get_aminoacidos()){
                // Imprime datos aminoácido(s).
                cout << "\n   " << aa.get_numero() << ".- Aminoácido ";
                cout << aa.get_nombre() << ":" << endl;

                for (Atomo a: aa.get_atomos()){
                    // Imprime datos átomos.
                    cout << "      " << a.get_numero() << ".- Átomo ";
                    cout << a.get_nombre();
                    cout << "\t\tCoordenadas: (";
                    cout << a.get_coordenada().get_x() << ", ";
                    cout << a.get_coordenada().get_y() << ", ";
                    cout << a.get_coordenada().get_z() << ")" << endl;
                }
            }
        }
        cout << endl;
    }

    cout << "Presione M o m para volver al menú." << endl;
    cout << "Presione cualquier otra tecla para salir." << endl;
    cout << "\nIngrese su elección: ";
    getline(cin, eleccion);

    if(eleccion == "M" || eleccion == "m"){
      menu();
    }

    else{
      system("clear");
      cout << "Que tenga un buen día..." << endl;
      system("sleep 1.5");
      exit(0);
    }
}

int main(){

    // Variables a utilizar.
    int accion;
    string id;
    string nombre;
    string cadenas;
    string aa;
    string aminoacidos;
    string elemento;
    string atomos;
    string x;
    string y;
    string z;
    char cadena_aux;
    string letra_aux;
    list<Proteina> proteinas;

    while (true){

        // Llamado función menú para imprimir pantalla inicial.
        accion = menu();

        if (accion == 1){

            system("clear");
            cout << "Favor ingrese los datos que se solicitan a continuación." << endl;
            cout << "Recordar que esto es referente a PROTEÍNAS." << endl;

            // Solicita ID y nombre de la proteína.
            cout << "\nID: ";
            getline(cin, id);
            cout << "Nombre: ";
            getline(cin, nombre);

            // Creación de la proteína sin cadenas.
            Proteina proteina = Proteina(id, nombre);

            // Solicita cantidad de cadenas de la proteína.
            cout << "Cantidad de cadenas: ";
            getline(cin, cadenas);

            // Asignación de letras (alfabéticamente) a las cadenas.
            // Para la cadena 1 se asigna la letra "A", para la 2,
            // la letra B y así sucesivamente.
            cadena_aux = 'A';

            // Ciclo con el cual se "rellenarán" las cadenas.
            for(int i = 0; i < stoi(cadenas); i++){
                letra_aux = cadena_aux + i;

                // Creación de la cadena sin aminoácidos.
                Cadena cadena = Cadena(letra_aux);

                // Solicita cantidad de aminoácidos de la cadena.
                cout << "Cantidad de aminoácidos de la cadena " << letra_aux << ": ";
                getline(cin, aminoacidos);

                // Ciclo con el cual se "rellenarán" los aminoácidos.
                for(int j = 0; j < stoi(aminoacidos); j ++){

                    // Solicita el/los nombres de los aminoácidos.
                    cout << "Nombre del aminoácido n° " << j + 1 <<": ";
                    getline(cin, aa);

                    // Creación del aminoácido sin átomos.
                    Aminoacido aminoacido = Aminoacido(aa, j + 1);

                    // Solicita cantidad de átomo(s) por aminoácido.
                    cout << "Cantidad de átomos del aminoácido: ";
                    getline(cin, atomos);

                    // Ciclo con el cual se "rellenarán" los átomos.
                    for (int k = 0; k < stoi(atomos); k++){

                        // Solicita nombre de los átomos que hay en cada
                        // aminoácido.
                        cout << "Nombre del atómo n° " << k + 1 <<": ";
                        getline(cin, elemento);

                        // Creación del átomo sin coordenadas.
                        Atomo atomo = Atomo(elemento, k + 1);

                        // Solicita las coordenadas para cada punto espacial 3D.
                        cout << "Coordenadas espaciales (x, y, z): " << endl;
                        cout << "Coordenada en x: ";
                        getline(cin, x);
                        cout << "Coordenada en y: ";
                        getline(cin, y);
                        cout << "Coordenada en z: ";
                        getline(cin, z);

                        // Se guardan las coordenadas del átomo.
                        atomo.set_coordenada(Coordenada(stod(x), stod(y), stod(z)));
                        // Se agrega el átomo a su respectivo aminoácido.
                        aminoacido.add_atomo(atomo);
                    }
                    // Cuando se ha ingresado la totalidad de aminoácidos (Con
                    // sus respectivos datos), estos se agregan a la cadena a la
                    // cual pertenecen.
                    cadena.add_aminoacido(aminoacido);
                }
                // Ahora, cuando se ha ingresado la totalidad de cadenas con sus
                // respectivos datos, estas se agregan a la proteína.
                proteina.add_cadena(cadena);
            }
            // Proteína con totos sus datos es agregada a la lista de proteínas.
            proteinas.push_back(proteina);
            continue;
        }

        else if(accion == 2){

            // Si el tamaño de la lista "proteinas" es mayor que 0, se
            // imprimirán los datos de esta.
            if (proteinas.size() > 0){
                imprimir_datos_proteina(proteinas);
            }
            // Del contrario, se comunicará al usuario que ninguna proteína ha
            // sido ingresada.
            else{
                system("clear");
                cout << "Aún no se ha ingresado ninguna proteína." << endl;
                system("sleep 1.5");
            }
            continue;
        }

        else if(accion == 3){
            // Cierre del programa.
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1.5");
            exit(0);
            break;
        }

        else{
            // "Reinicio" del programa por una acción inválida.
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            main();
        }
    }

    return 0;
}
