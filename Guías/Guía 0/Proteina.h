#include <iostream>
#include <list>
#include "Cadena.h"

using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina{

    private:
        string nombre;
        string id;
        list<Cadena> cadenas;

    public:
        // Constructor.
        Proteina(string id, string nombre);

        // Métodos.
        void set_id(string id);
        void set_nombre(string nombre);
        void add_cadena(Cadena cadena);
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
};
#endif
