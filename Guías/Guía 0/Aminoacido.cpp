#include <iostream>
#include <list>
#include "Aminoacido.h"
#include "Atomo.h"

using namespace std;

// Constructor clase Aminoacido.
Aminoacido::Aminoacido(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

// Métodos set.
void Aminoacido::set_nombre(string nombre){
    this->nombre = nombre;
}

void Aminoacido::set_numero(int numero){
    this->numero = numero;
}

void Aminoacido::add_atomo(Atomo atomo){
    // Se agrega cada átomo a la lista "atomos".
    this->atomos.push_back(atomo);
}

// Métodos get.
string Aminoacido::get_nombre(){
    return this->nombre;
}

int Aminoacido::get_numero(){
    return this->numero;
}

list<Atomo> Aminoacido::get_atomos(){
  return this->atomos;
}
