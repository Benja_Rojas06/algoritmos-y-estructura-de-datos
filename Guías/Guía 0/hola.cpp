#include <iostream>
#include <stdlib.h>
#include <list>
#include "Proteina.h"
#include "Aminoacido.h"
#include "Cadena.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

int menu(){

    string opcion;
    system("clear");

    cout << "+-------------------------------------------------------+" << endl;
    cout << "|------------------------LAB 0--------------------------|" << endl;
    cout << "+-------------------------------------------------------+" << endl;

    cout << "¿Que desea hacer?" << endl;
    cout << "1.- Ingresar una proteína" << endl;
    cout << "2.- Revisar datos sobre las proteínas ingresadas" << endl;
    cout << "3.- Nada (salir)" << endl;

    getline(cin, opcion);

    return stoi(opcion);
}

void imprimir_datos_proteina(list<Proteina> proteinas){

    string aux;

    cout << "Las proteínas ingresadas son las siguientes:" << endl << endl;

    for (Proteina p: proteinas){
        cout << "Nombre: " << p.get_nombre() << endl;
        cout << "ID: " << p.get_id() << endl << endl;

        for (Cadena c: p.get_cadenas()){
            cout << "Cadena " << c.get_letra() << ":" << endl;

            for (Aminoacido a: c.get_aminoacidos()){
                cout << "   " << a.get_numero() << "- " << a.get_nombre() << ":" << endl;

                for (Atomo at: a.get_atomos()){
                    cout << "       " << at.get_numero() << ".- " << at.get_nombre();
                    cout << "           Coordenadas: (";
                    cout << at.get_coordenada().get_x() << ",";
                    cout << at.get_coordenada().get_y() << ",";
                    cout << at.get_coordenada().get_z() << ")" << endl;
                }
            }
        }
        cout << endl;
    }

    cout << "Presione enter para continuar...";
    getline(cin, aux);

}

int main(){

    int opcion;
    string x, y, z;
    string aa;
    string aminoacidos;
    string cadenas;
    string id;
    string nombre;
    string atomos;
    string elemento;
    char char_aux;
    string aux_letra;
    list<Proteina> proteinas;

    while (1){

        opcion = menu();

        if (opcion == 1){

            // Se pide nombre id y nombre de proteína a ingresar
            cout << "¿Cuál es la ID de su proteína? ";
            getline(cin, id);
            cout << "¿Cuál es el nombre de su proteína? ";
            getline(cin, nombre);

            // Con los datos obtenidos se crea una proteína sin cadenas
            Proteina proteina = Proteina(id, nombre);

            cout << "¿Cuantas cadenas posee su proteína? ";
            getline(cin, cadenas);

            // Se asignarán letras del abecedario partiendo desde la letra A
            // como se hace de forma estándar
            char_aux = 'A';
            for(int i=0; i<stoi(cadenas); i++){
                aux_letra = char_aux+i;

                // Teniendo una letra, se crea una cadena sin aminoácidos
                Cadena cadena = Cadena(aux_letra);

                cout << "¿Cuantos aminoácidos tiene la cadena " << aux_letra << "? ";
                getline(cin, aminoacidos);

                for(int j=0; j<stoi(aminoacidos); j++){
                    cout << "¿Cual es el nombre de aminoacido numero " << j+1 <<"? ";
                    getline(cin, aa);

                    // Teniendo un número de aminoácidos, se crean a partir del
                    // número 1
                    Aminoacido aminoacido = Aminoacido(aa, j+1);

                    cout << "¿Cuántos átomos tiene este aminoácido? ";
                    getline(cin, atomos);

                    for (int k=0; k<stoi(atomos); k++){

                        // Se pregunta el nombre del átomo y sus coordenadas,
                        // a continuación los átomos se ingresan a un aminoácido
                        // previamente creado (sin átomos)
                        cout << "¿Cuál es el nombre del átomo n° " << k+1 <<"? ";
                        getline(cin, elemento);

                        Atomo atomo = Atomo(elemento, k+1);

                        cout << "¿Cuáles son sus coordenadas x, y, z?" << endl;
                        getline(cin, x);
                        getline(cin, y);
                        getline(cin, z);

                        atomo.set_coordenada(Coordenada(stod(x), stod(y), stod(z)));
                        aminoacido.add_atomo(atomo);

                    }

                    // Cuando un aminoácido ya posee todos sus átomos con sus
                    // respectivas coordenadas, este se agrega a la cadena
                    // previamente creada.
                    cadena.add_aminoacido(aminoacido);
                }

                // Cuando el proceso anterior se repitió las veces necesarias y
                // la cadena posee sus aminoácidos creados, la cadena se agrega
                // a la proteína creada anteriormente.
                proteina.add_cadena(cadena);
            }

            // Finalmente cuando una proteína esta lista con sus cadenas, se
            // agrega a la lista de proteínas.
            proteinas.push_back(proteina);
            continue;
        }



        else if(opcion == 2){

            // Se llama a funcion que imprime datos y se le envia la lista de
            // proteínas, solo si existe al menos 1 proteína para ser cargada

            if (proteinas.size() > 0){
                imprimir_datos_proteina(proteinas);
            }
            else{
                cout << "No existen proteínas para visualizar..." << endl;
                system("sleep 2");
            }

            continue;
        }

        else if(opcion == 3){
            exit(0);
            break;
        }

        else{
            cout << "Opción no válida";
            opcion = menu();
        }
    }

    return 0;
}
