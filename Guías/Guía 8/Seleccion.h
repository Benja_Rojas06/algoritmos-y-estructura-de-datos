#include <iostream>

using namespace std;

#ifndef SELECCION_H
#define SELECCION_H


// Creación de la clase Seleccion.
class Seleccion{

    public:
        // Constructor por defecto de la clase Seleccion.
        Seleccion();
        // Función encargada de realizar el método de ordenamiento Seleccion.
        double selec_ordena(int *array, int n_elementos);
};
#endif
