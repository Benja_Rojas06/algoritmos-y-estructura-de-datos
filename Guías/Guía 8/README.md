# Propuesta solución Guía 8:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa que realiza dos métodos de ordenamiento interno, el
método "Selección" y "Quicksort". Esto gracias a un arreglo que se llena con
números aleatorios y, que, posteriormente, se procede a ordenar. Además, se
indica el timepo de ejecución de estos métodos.


## Funcionamiento e Interacción:

- El programa se inicializa ingresando los dos parámetros (El primero corresponde
al largo del arreglo a generar y el segundo a si se desea o no visualizar el arreglo
original y el ordenado por cada método) seguidamente del comando "./Programa" en
la terminal, con el cual se ejecuta el mismo. Un ejemplo es: "./Programa 10 s".
De este modo se le indica al programa que el largo del arreglo será de 10 y que
sí se desea visualizar el arreglo original y el ordenado por ambos métodos. Del
mismo modo, si no se desea visualizar ambos arreglos, bastará con ingresar "n"
luego de indicar el largo del arreglo. Posterior a ello, se procede a cerrar
automáticamente el programa.


## Fallos que posee el programa:

- No se implementa la librería "chronos" para calcular el tiempo de ejecución de
los ordenamientos.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en la guía.
- Se ordena un mismo conjunto de elementos con ambos métodos de ordenamiento.
- Se entregan los tiempos en milisegundos.
- Se realizan las validaciones correspondientes para cada parámetro ingresado a
modo de que el progrma funcione correctamente.
- Se debe realizar la instalación de "make" para que no ocurra ningún problema
en la compilación, esto con "sudo apt install make".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa" seguido de los parámetros.
Ej: "./Programa 10 s".
