#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <ctime>
#include "Quicksort.h"
#include "Seleccion.h"

using namespace std;


// Función encargada de imprimir los timepos que demora la ejecución de
// los ordenamientos "Selección" y "Quicksort".
void imprimir_tiempos(double *tiempo, string *met_ordena){

    // Impresiones estéticas.
    system("clear");
    cout << "\t\t.~Métodos de Ordenamiento~." << endl << endl;
    cout << "-----------------------------------------------------------" << endl;
    cout << "\tMétodo             |\t\tTiempo" << endl;
    cout << "-----------------------------------------------------------" << endl;

    // Ciclo para imprimir los tiempos correlativos a cada uno de los métodos.
    for(int i = 0; i < 2; i++){
        // Se procede a imprimir el método Selección y, posterior a este, el
        // método Quicksort.
        cout << "\t" << met_ordena[i] << "          |\t    " << tiempo[i] << " milisegundos" << endl;
    }
    // Se "cierra" la tabla de los tiempos de ejecución.
    cout << "-----------------------------------------------------------" << endl;
}


// Función encargada de imprimir el arreglo original y los ordenados si así
// se ha indicado en el parámetro "ver".
void imprimir_arrays(int n_elementos, string ver){

    // Creación de variables a utilizar.
    int random;
    Seleccion met_seleccion;
    Quicksort met_quicksort;
    int arreglo[3][n_elementos];
    double tiempo[2];

    // Se indica que los números aleatorios generados no se repitan (No funciona).
    srand((unsigned)time(0));

    // Ciclo con el cual se irán generando los números aleatorios.
    for(int i = 0; i < n_elementos; i++){
        // Generación de números aleatorios.
        random = rand()%n_elementos + 1;

        // Ciclo para agregar los números aleatorios al arreglo.
        for(int j = 0; j < 3; j++){
            // Se procede a rellenar el arreglo con los números aleatorios
            // generados uno por uno.
            arreglo[j][i] = random;
        }
    }

    // Se realiza el ordenamiento por selección y, posterior a ello, se retorna
    // el tiempo que ha demorado esta acción.
    tiempo[0] = met_seleccion.selec_ordena(arreglo[0], n_elementos);
    // Se realiza el ordenamiento por quicksort y, posterior a ello, se retorna
    // el tiempo que ha demorado esta acción.
    tiempo[1] = met_quicksort.quick_ordena(arreglo[1], n_elementos);

    // Arreglo que almacena los nombres de los métodos de ordenamiento.
    string met_ordena[2] = {"Seleccion", "Quicksort"};

    // Llamado función encargada de imprimir los tiempos.
    imprimir_tiempos(tiempo, met_ordena);

    // Si el parámetro "ver" es igual a "s".
    if(ver == "s"){
        cout << "\n" << endl;
        // Se indica que se imprimirá el arreglo original.
        cout << "Arreglo Original:" << endl << "| ";

        // Se procede a imprimir el arreglo original.
        for(int i = 0; i < n_elementos; i++){
            // Impresión se realiza posición por posición hasta llegar al
            // último elemento.
            cout << arreglo[2][i] << " | ";
        }
        cout << endl << endl;
        // Se indica que se imprimirá el arreglo ordenado.
        cout << "Arreglo Ordenado:" << endl;
        cout << "------------------------------------------------------------------------------" << endl;

        // Se procede a imprimir el arreglo ordenado en sus dos formas.
        for(int i = 0; i < 2; i++){
            // Se imprime el nombre del método gracias a nuestro arreglo
            // creado con anterioridad.
            cout << "\t" << met_ordena[i] << "          |\t    " << "| ";

            for(int j = 0; j < n_elementos; j++){
                // Se imprime el arreglo posición por posición hasta llegar al
                // último elemento.
                cout << arreglo[i][j] << " | ";
            }
            // Salto de línea.
            cout << endl;
        }
        // Estética para cerrar la "Tabla".
        cout << "------------------------------------------------------------------------------" << endl;
    }
}


// Función principal con la cual se realizan las validaciones de los dos
// parámetros que se deben ingresar.
int main(int argc, char **argv){

    // Cantidad de elementos a ordenar.
    int n_elementos;
    // Visualización (Si o No).
    string ver;

    // Se indica que la cantidad de parámetros a ingresar es 2.
    if(argc != 3){
        cout << "\nPor favor, para iniciar el programa ingrese los 2 parámetros." << endl;
        cout << "El primero corresponde al tamaño del arreglo a generar y el";
        cout << " segundo a si desea viasualizar el arreglo original y el ordenado." << endl << endl;
        cout << "Ejemplo de ejecución: './Programa 10 s'" << endl << endl;
    }

    // Sino
    else{
        // Se intenta asignar valores correctos.
        try{
            // n_elementos toma la posición 1.
            n_elementos = stoi(argv[1]);
            // ver toma la posición 2.
            ver = argv[2];

            // Se indica que la cantidad de elementos debe ser si o sí mayor que
            // 1 y menor que 1.000.000.
            if(n_elementos > 1000000 || n_elementos < 1){
                cout << "\nEl valor: '" << n_elementos << "' es inválido." << endl;
                cout << "Recuerde que debe ser un valor positivo menor o igual ";
                cout << "que 1.000.000." << endl << endl;
                // Cierre del programa.
                exit(1);
            }

            // Se indica que solo se puede seleccionar "s" para visualizar el
            // contenido de los vectores o "n" para no hacerlo.
            else if((ver != "n") && (ver != "s")){
                cout << "\nEl parámetro ingresado para la visualización no es válido." << endl;
                cout << "Debe ingresar 's' (Visualizar) o 'n' (No Visualizar)." << endl << endl;
                // Cierre del programa.
                exit(1);
            }
        }

        // Sino, se indica que existe un error, puesto que el primer parámetro
        // ingresado debe ser si o sí un número.
        catch(const std::invalid_argument& e){
            cout << "\nError: El primer parámetro debe ser un número." << endl << endl;
            // Cierre del programa.
            exit(1);
        }
        // Llamado de la función con la cual se imprime el arreglo original,
        // los ordenamientos y los tiempos.
        imprimir_arrays(n_elementos, ver);
    }
    return 0;
}
