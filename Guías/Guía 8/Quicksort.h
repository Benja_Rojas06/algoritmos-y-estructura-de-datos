#include <iostream>

using namespace std;

#ifndef QUICKSORT_H
#define QUICKSORT_H


// Creación de la clase Quicksort.
class Quicksort{

    private:
        // Función encargada de realizar la partición del método de
        // ordenamiento Quicksort.
        void quick_reduce(int ini, int fin, int &pos, int *array);

    public:
        // Constructor por defecto.
        Quicksort();
        // Función encargada de realizar el método de ordenamiento Quicksort.
        double quick_ordena(int *array, int n_elementos);
};
#endif
