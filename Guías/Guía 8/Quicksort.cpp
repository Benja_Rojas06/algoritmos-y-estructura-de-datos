#include <iostream>
#include <time.h>
#include "Quicksort.h"

using namespace std;


// Constructor por defecto de la clase Quicksort.
Quicksort::Quicksort(){}


// Función que se encaragará de realizar la partición del
// método de ordenamiento Quicksort.
void Quicksort::quick_reduce(int ini, int fin, int &pos, int *array){

    // Creación de las variables a utilizar.
    int aux;
    int izq = ini;
    int der = fin;
    bool band = true;

    // Asignación de la posición inicial.
    pos = ini;

    // Cuando nuestra banda sea "verdadera".
    while(band == true){
        // Cuando nuestro arreglo en la posición actual sea menor o igual al
        // arreglo por derecha y, además, la posición actual sea diferente a la
        // por derecha.
        while((array[pos] <= array[der]) && (pos != der)){
            // Valor por derecha disminuye en 1.
            der = der - 1;
        }

        // Si la posición actual es igual al valor por derecha.
        if(pos == der){
            // band se convierte en falso.
            band = false;
        }

        // Sino habrá una reasignación de valores.
        else{
            // Auxiliar toma el valor del arreglo en la posición actual.
            aux = array[pos];
            // Arreglo en la posición actual tome el valor del arreglo por derecha.
            array[pos] = array[der];
            // Arreglo por derecha toma el valor del auxiliar.
            array[der] = aux;
            // Posición actual toma el valor por derecha.
            pos = der;

            // Cuando nuestro arreglo en la posición actual sea mayor o igual al
            // arreglo por izquierda y, además, la posición actual sea diferente
            // a la por izquierda.
            while((array[pos] >= array[izq]) && (pos != izq)){
                // Valor por izquierda aumenta en 1.
                izq = izq + 1;

                // Si la posición actual es igual al valor por izquierda.
                if(pos == izq){
                    // band se convierte en falso.
                    band = false;
                }

                // Sino habrá una reasignación de valores.
                else{
                    // Auxiliar toma el valor del arreglo en la posición actual.
                    aux = array[pos];
                    // Arreglo en la posición actual tome el valor del
                    // arreglo por izquierda.
                    array[pos] = array[izq];
                    // Arreglo por izquierda toma el valor del auxiliar.
                    array[izq] = aux;
                    // Posición actual toma el valor por izquierda.
                    pos = izq;
                }
            }
        }
    }
}


// Función que realizará el ordenamiento del método Quicksort.
double Quicksort::quick_ordena(int *array, int n_elementos){

    // Creación de las variables a utilizar.
    clock_t inicio = clock();
    int pila_menor[n_elementos];
    int pila_mayor[n_elementos];
    int ini;
    int fin;
    int pos;
    int tope;

    // Asignación de valores a las variables.
    tope = 0;
    pila_menor[tope] = 0;
    pila_mayor[tope] = n_elementos - 1;

    // Para que se pueda proceder a ordenar, primero el tope debe ser mayor
    // o igual a 0.
    while(tope >= 0){
        // Valor inicial (ini) será igual al de la la pila menor.
        ini = pila_menor[tope];
        // Valor final (fin) será igual al de la la pila mayor.
        fin = pila_mayor[tope];
        // Nuestro tope adquiere un nuevo valor.
        tope = tope - 1;
        // Llamado recursivo de la función quick_reduce para particionar.
        quick_reduce(ini, fin, pos, array);

        // Si el valor inicial es menor que la posición actual menos 1.
        if(ini < (pos - 1)){
            // El tope aumenta en 1.
            tope = tope + 1;
            // La pila menor adquiere el valor inicial.
            pila_menor[tope] = ini;
            // La pila mayor adquiere el valor actual menos 1.
            pila_mayor[tope] = pos - 1;
        }

        // Si el valor final es mayor que la posición actual más 1.
        if(fin > (pos + 1)){
            // El tope aumenta en 1.
            tope = tope + 1;
            // La pila menor adquiere el valor actual más 1.
            pila_menor[tope] = pos + 1;
            // La pila mayor adquiere el valor final.
            pila_mayor[tope] = fin;
        }
    }
    // Se retorna el tiempo demorado y, además, este se multiplica por 1.000
    // para pasar de "microsegundos" a "milisegundos".
    return (double)(clock()-inicio)/CLOCKS_PER_SEC * 1000;
}
