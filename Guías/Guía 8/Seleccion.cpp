#include <iostream>
#include <time.h>
#include "Seleccion.h"

using namespace std;


// Constructor por defecto de la clase Seleccion.
Seleccion::Seleccion(){}


// Función que realizará el ordenamiento del método Seleccion.
double Seleccion::selec_ordena(int *array, int n_elementos){

    // Creación de las variables a utilizar.
    clock_t inicio = clock();
    int menor;
    int k;

    // Ciclo con el cual se recorrerá el arreglo desde la posición 0 hasta la
    // posición "n_elementos" menos 1.
    for(int i = 0; i <= (n_elementos - 1); i++){

        // La posición "i" en el arreglo pasará a ser la posición menor.
        menor = array[i];
        // Asigación del valor de k (Toma i).
        k = i;

        // Ciclo con el cual se recorrerá el arreglo desde la posición "i + 1"
        // hasta la posición "n_elementos".
        for(int j = (i + 1); j < n_elementos ; j++){
            // Si la posición en "j" del arreglo es menor que el menor valor.
            if(array[j] < menor){
                // Se asignará como nuevo valor menor la posición del arreglo en j.
                menor = array[j];
                // Reasignación del valor de k (Toma j).
                k = j;
            }
        }

        // Arreglo en la posición k toma el valor del arreglo en la posición i.
        array[k] = array[i];
        // Arreglo en la posición i toma el valor del menor valor.
        array[i] = menor;
    }
    // Se retorna el tiempo demorado y, además, este se multiplica por 1.000
    // para pasar de "microsegundos" a "milisegundos".
    return (double)(clock()-inicio)/CLOCKS_PER_SEC * 1000;
}
