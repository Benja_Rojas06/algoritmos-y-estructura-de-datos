#include <iostream>
#include "ABB.h"

using namespace std;


// Constructor por defecto de la clase 'Árbol'.
Arbol::Arbol(){}


// Creación de los nodos.
Nodo* Arbol::crear_nodo(int numero, Nodo *progenitor){

    Nodo *temp;

    temp = new Nodo;

    // Asignación de un valor al nodo creado.
    temp->dato = numero;

    // Creación de los "subárboles" izquierdo y derecho.
    temp->izq = NULL;
    temp->der = NULL;
    temp->nodo_progenitor = progenitor;

    return temp;
}


// Se agregan los nodos en su subárbol correspondiente.
void Arbol::agregar(Nodo *&raiz, int numero, Nodo *progenitor){

    // Variable para comparar el valor de la raíz con lo ingresado.
    int valor_raiz;

    // Si raíz no tiene valor asignado, el primer nodo ingresado se
    // convertirá en la raíz.
    if(raiz == NULL){
        Nodo *temp;
        temp = crear_nodo(numero, progenitor);
        raiz = temp;
        agregar(raiz, numero, progenitor);
    }

    // De lo contrario, no se ingresará en la raíz, sino que en un subárbol.
    else{
        valor_raiz = raiz->dato;

        // Si el número (Nodo) a ingresar es menor que la raíz, el nodo se
        // agregará al lado izquierdo.
        if(numero < valor_raiz){
            agregar(raiz->izq, numero, raiz);
        }

        // Ahora bien, si el número a ingresar es mayor que la raíz, este se
        // agregará por la derecha.
        else if(numero > valor_raiz){
            agregar(raiz->der, numero, raiz);
        }
    }
}


// Se retorna el nodo más pequeño.
Nodo* Arbol::encontrar_por_izquierda(Nodo *raiz){

    if(raiz->izq){
        return encontrar_por_izquierda(raiz->izq);
    }

    else{
        return raiz;
    }
}


// "Destrucción" de los nodos por izquierda y por derecha.
void Arbol::destruir_nodo(Nodo *nodo){
    // Reasignación de los valores por izquierda a "NULL".
    nodo->izq = NULL;
    // Reasignación de los valores por derecha a "NULL".
    nodo->der = NULL;
}


// Reemplazo de un nodo progenitor por su hijo.
void Arbol::reemplazar_hijo(Nodo *raiz, Nodo *reemplazo){

    if(raiz->nodo_progenitor){
        // Nodo progenitor se cambia por hijo izquierdo.
        if(raiz->dato == raiz->nodo_progenitor->izq->dato){
            raiz->nodo_progenitor->izq = reemplazo;
        }

        // Nodo progenitor se cambia por hijo derecho.
        else if(raiz->dato == raiz->nodo_progenitor->der->dato){
            raiz->nodo_progenitor->der = reemplazo;
        }

    }

    // Como se ha cambiado el nodo progenitor por su hijo, el primero ya no
    // existe. Es por ello que el nodo hijo pasa a ser el nodo progenitor.
    if(reemplazo){
        reemplazo->nodo_progenitor = raiz->nodo_progenitor;
    }
}


// Eliminación de un nodo.
void Arbol::eliminar_nodo(Nodo *&nodo_a_eliminar){

    // Si el nodo a eliminar tiene 2 hijos (Por izquierda y por derecha).
    if(nodo_a_eliminar->izq && nodo_a_eliminar->der){
        // Se busca el nodo más pequeño para reemplazarlo por
        // el "nodo_progenitor".
        Nodo *menor = encontrar_por_izquierda(nodo_a_eliminar->der);
        nodo_a_eliminar->dato = menor->dato;
        eliminar_nodo(menor);
    }

    // Solamente un hijo y por el lado izquierdo.
    else if(nodo_a_eliminar->izq){
        reemplazar_hijo(nodo_a_eliminar, nodo_a_eliminar->izq);
        destruir_nodo(nodo_a_eliminar);
    }

    // Solamente un hijo y por el lado derecho.
    else if(nodo_a_eliminar->der){
        reemplazar_hijo(nodo_a_eliminar, nodo_a_eliminar->der);
        destruir_nodo(nodo_a_eliminar);
    }

    // El nodo no tiene hijos.
    else{
        reemplazar_hijo(nodo_a_eliminar, NULL);
    }
}


// Función para ayudar en la eliminación.
void Arbol::eliminacion(Nodo *&raiz, int numero){

    // Si el número a eliminar es menor que la raíz.
    if(numero < raiz->dato){
        // Eliminación por la izquierda.
        eliminacion(raiz->izq, numero);
    }

    // Si el número a eliminar es mayor que la raíz.
    else if(numero > raiz->dato){
        // Eliminación por la derecha.
        eliminacion(raiz->der, numero);
    }

    // Si es "igual", se eliminará la raíz.
    else{
        eliminar_nodo(raiz);
    }
}


// Función para imprimir el Árbol en preorden.
void Arbol::imprimir_preorden(Nodo *raiz){
    // Si la raíz existe, se procederá a imprimir por izquierda y por derecha.
    if(raiz != NULL){
        cout << raiz->dato << " - ";
        // Impresión por izquierda.
        imprimir_preorden(raiz->izq);
        // Impresión por derecha.
        imprimir_preorden(raiz->der);
    }
}


// Función para imprimir el Árbol en inorden.
void Arbol::imprimir_inorden(Nodo *raiz){
    // Si la raíz existe, se procederá a imprimir por izquierda y por derecha.
    if(raiz != NULL){
        // Impresión por izquierda.
        imprimir_inorden(raiz->izq);
        cout << raiz->dato << " - ";
        // Impresión por derecha.
        imprimir_inorden(raiz->der);
    }
}


// Función para imprimir el Árbol en posorden.
void Arbol::imprimir_posorden(Nodo *raiz){
    // Si la raíz existe, se procederá a imprimir por izquierda y por derecha.
    if(raiz != NULL){
        // Impresión por izquierda.
        imprimir_posorden(raiz->izq);
        // Impresión por derecha.
        imprimir_posorden(raiz->der);
        cout << raiz->dato << " - ";
    }
}
