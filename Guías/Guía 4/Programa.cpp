#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "ABB.h"

using namespace std;


// Clase "Grafo" con la cual se recorrerá el Árbol y se
// creará su grafo correspondiente.
class Grafo{

    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la clase Grafo.
        Grafo(Nodo *raiz){
            this->arbol = raiz;
        }

    // Recorrido del Árbol.
    void recorrer_arbol(Nodo *p, ofstream &archivo){

        string cadena;

        // Enlazamiento de los nodos al grafo.
        if(p != NULL){

            // Enlazamiento por izquierda.
            if(p->izq != NULL){
                // Se escribe el nodo dentro del archivo.
                archivo << p->dato << " -> " << p->izq->dato << ";" << endl;
            }

            else{
                cadena = to_string(p->dato) + "i";
                cadena = "\"" + cadena + "\"";
                archivo << cadena << "[shape=point]" << endl;
                archivo << p->dato << " -> " << cadena << ";" << endl;
            }

            // Enlazamiento por derecha.
            if(p->der != NULL){
                // Se escribe el nodo dentro del archivo.
                archivo << p->dato << " -> " << p->der->dato << ";" << endl;
            }

            else{
                cadena = to_string(p->dato) + "d";
                cadena = "\"" + cadena + "\"";
                archivo << cadena << "[shape=point]" << endl;
                archivo << p->dato << " -> " << cadena << ";" << endl;
            }

            // Llamado para el recorrido del Árbol por izquierda y por derecha
            // para la creación del grafo.
            recorrer_arbol(p->izq, archivo);
            recorrer_arbol(p->der, archivo);
        }
        return;
    }

    // Creación del grafo.
    void crear_grafo(){

        ofstream archivo;

        // Creación (Apertura) del archivo "datos.txt" con el cual se
        // generará el grafo.
        archivo.open("datos.txt");
        // Dentro del archivo creado se escribe "digraph G {".
        archivo << "digraph G {" << endl;
        // Se escoge un color a modo de personalizar los nodos.
        archivo << "node [style=filled fillcolor=green];" << endl;

        // Se llama a la función "recorrer_arbol" para que genere el archivo
        // de texto y, así, poder generar el grafo.
        recorrer_arbol(this->arbol, archivo);

        // Impresión de "}" para denotar que ya no se escribirá más.
        archivo << "}" << endl;
        // Cierre del archivo.
        archivo.close();

        // Generación del grafo.
        system("dot -Tpng -ografo.png datos.txt &");
        // Visualización del grafo.
        system("eog grafo.png &");
    }
};


// Función que muestra los tipos de recorridos que se pueden realizar.
int opc_recorrido(){

    int recorrido;

    // Opciones del menú.
    system("clear");
    cout << "\t.~Bienvenido a los tipos de recorrido del ABB~." << endl << endl;
    cout << "¿Que tipo de recorrido desea realizar?" << endl << endl;
    cout << "1.- Recorrido Preorden." << endl;
    cout << "2.- Recorrido Inorder." << endl;
    cout << "3.- Recorrido Posorder." << endl;
    cout << "4.- Volver al menú inicial." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    cin >> recorrido;

    return recorrido;
}


// Función con la cual se recorre el Árbol de las 3 maneras posibles.
void tipo_recorrido(Nodo *raiz, Arbol *arbol){

    int recorrido;
    int salir;

    do{

        // Llamado función menú para imprimir las opciones del menú.
        recorrido = opc_recorrido();

        // Siempre y cuando el Árbol haya sido creado, este se podrá recorrer.
        if(raiz != NULL){

            // Se recorre el Árbol en Preorden.
            if(recorrido == 1){
                system("clear");
                cout << "\t.~Árbol Recorrido en Preoden~." << endl << endl;
                // Impresión del Árbol recorrido en "Preorden".
                arbol->imprimir_preorden(raiz);

                // Retorno al menú del tipo de recorrido presionando cualquier número.
                cout << "\n\nPresione cualquier número para volver al menú anterior: ";
                cin >> salir;

                // Condicionales para volver al menú.
                if(salir == 0){
                    tipo_recorrido(raiz, arbol);
                }
                else{
                    tipo_recorrido(raiz, arbol);
                }
                break;
            }

            // Se recorre el Árbol en Inorden.
            else if(recorrido == 2){
                system("clear");
                cout << "\t.~Árbol Recorrido en Inorden~." << endl << endl;
                // Impresión del Árbol recorrido en "Inorden".
                arbol->imprimir_inorden(raiz);

                // Retorno al menú del tipo de recorrido presionando cualquier número.
                cout << "\n\nPresione cualquier número para volver al menú anterior: ";
                cin >> salir;

                // Condicionales para volver al menú.
                if(salir == 0){
                    tipo_recorrido(raiz, arbol);
                }
                else{
                    tipo_recorrido(raiz, arbol);
                }
                break;
            }

            // Se recorre el Árbol en Posorden.
            else if(recorrido == 3){
                system("clear");
                cout << "\t.~Árbol Recorrido en Posorden~." << endl << endl;
                // Impresión del Árbol recorrido en "Posorden".
                arbol->imprimir_posorden(raiz);
                // Retorno al menú del tipo de recorrido presionando cualquier número.
                cout << "\n\nPresione cualquier número para volver al menú anterior: ";
                cin >> salir;

                // Condicionales para volver al menú.
                if(salir == 0){
                    tipo_recorrido(raiz, arbol);
                }
                else{
                    tipo_recorrido(raiz, arbol);
                }
                break;
            }
        }

        // Si no ha sido creado, este no se puede recorrer.
        else{
            cout << "\nEl Árbol aún no ha sido creado (rellenado)." << endl;
        }
    }
    while(recorrido != 4);
}

// Función con la cual se valida que lo ingresado por el usuario sea, solamente,
// un número, esto para, posteriormente, agregarlo al Árbol.
bool validacion(string entrada){

    bool validado = true;

    for(int i = 0; i < entrada.size(); i++){
        // Valores correspondientes al código ascii:
        //      48 = 0       //       57 = 9
        if((entrada[i] < 48) || (entrada[i] > 57)){
            validado = false;
        }
    }
    return validado;
}


// Función con la cual se pueden eliminar nodos (Números) al Árbol.
void eliminar_nodo(Nodo *raiz, Arbol *arbol){

    bool num_validado;
    string numero;

    cout << "\nIngrese el número (Nodo) que desea eliminar: ";
    cin.ignore();
    getline(cin, numero);

    // Se envía lo ingresado para validar que este sea un número.
    num_validado = validacion(numero);


    // Si lo ingresado por el usuario se valida como un número, se procede
    // a eliminarlo del Árbol.
    if(num_validado == true){
        // Eliminación n+umero (Nodo) ingresado.
        arbol->eliminacion(raiz, stoi(numero));
    }

    // Del modo contrario (Si no es un número), se informará de aquello
    // al usuario.
    else{
        cout << "\nEl nodo '" << numero << "' ";
        cout << "ingresado, no se ha podido eliminar porque no ";
        cout << "corresponde a un número entero." << endl;
        system("sleep 2.5");
    }
}


// Función con la cual se pueden agregar nodos (Números) al Árbol.
void agregar_nodo(Nodo *&raiz, Arbol *arbol){

    bool num_validado;
    string numero;
    Nodo *nodo = NULL;

    system("clear");
    cout << "\nIngrese un número (Nodo) que desea agregar: ";
    cin.ignore();
    getline(cin, numero);

    // Se envía lo ingresado para validar que este sea un número.
    num_validado = validacion(numero);

    // Si lo ingresado por el usuario se valida como un número, procede
    // a agregarlo al Árbol.
    if(num_validado == true){
        nodo = arbol->crear_nodo(stoi(numero), NULL);

        // Inicialización de la raíz del Árbol.
        // Si no existe ninguna raíz, el primer nodo pasará a ser la raíz.
        if(raiz == NULL){
            // Primer nodo se convierte en la raíz.
            raiz = nodo;
        }

        // Si la raíz ya existe, simplemente se agrega el nodo.
        else{
            // Se agrega el nuevo nodo.
            arbol->agregar(raiz, stoi(numero), NULL);
        }
    }

    // Del modo contrario (Si no es un número), se informará de aquello
    // al usuario.
    else{
        cout << "\nEl nodo '" << numero << "' ";
        cout << "ingresado, no se ha podido agregar porque no ";
        cout << "corresponde a un número entero." << endl;
        system("sleep 2.5");
    }
}


// Función que muestra las opciones que posee el menú.
int opc_menu(){

    int accion;

    // Opciones del menú.
    system("clear");
    cout << "\t.~Bienvenido al generador de ABB~." << endl << endl;
    cout << "¿Que acción desea realizar sobre el Árbol?" << endl << endl;
    cout << "1.- Agregar nodo (Número)." << endl;
    cout << "2.- Eliminar nodo." << endl;
    cout << "3.- Modificar nodo." << endl;
    cout << "4.- Mostrar el contenido del Árbol." << endl;
    cout << "5.- Generar el grafo." << endl;
    cout << "6.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    cin >> accion;

    return accion;
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(Nodo *raiz, Arbol *arbol){

    int accion;

    while (true){

        // Llamado función menú para imprimir las opciones del menú.
        accion = opc_menu();

        // Se agregan los nodos (Números) al Árbol.
        if(accion == 1){
            system("clear");
            // Creación del nodo e ingreso del nodo (Número).
            agregar_nodo(raiz, arbol);
        }

        // Se eliminan los nodos (Números) del Árbol.
        else if(accion == 2){
            system("clear");
            // Eliminación de un nodo (Número) previamente seleccionado.
            eliminar_nodo(raiz, arbol);
        }

        // Se modifican los nodos (Números) del Árbol. Para ello, se elimina el
        // nodo y, posteriormente, se agrega uno nuevo en la misma ubicación.
        // Cabe mencionar que esta opción no funciona bien, ya que solo se logra
        // la eliminación, mas no la incorporación del nuevo nodo.
        else if(accion == 3){
            system("clear");
            // Eliminación de un nodo (Número) previamente seleccionado.
            eliminar_nodo(raiz, arbol);
            // Creación del nodo e ingreso del nodo (Número).
            agregar_nodo(raiz, arbol);
        }

        // Se muestra el Árbol que se tiene hasta el momento.
        else if(accion == 4){
            system("clear");
            // Llamado de la función que desplegará un menú con los distintos
            // tipos de recorrido que tiene el Árbol.
            tipo_recorrido(raiz, arbol);
        }

        // Se genera el grafo correspondiente al Árbol.
        else if(accion == 5){
            system("clear");
            // Representación gráfica (Grafo) correspondiente al Árbol.
            Grafo *grafo = new Grafo(raiz);
            grafo->crear_grafo();
        }

        // Cierre del programa.
        else if(accion == 6){
            // Mensaje de despedida.
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            // Se cierra el programa.
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            opc_menu();
        }
    }
}


// Función principal que define la raíz y el árbol nuevo. Además, se encarga de
// llamar a la función "menú" para el "inicio" del programa.
int main(){

    // Definición de la raíz.
    Nodo *raiz = NULL;
    // Definición del árbol nuevo.
    Arbol *arbol = new Arbol();

    // Llamado a la función menú.
    menu(raiz, arbol);

    return 0;
}
