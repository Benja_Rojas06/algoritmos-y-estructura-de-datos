# Propuesta solución Guía 4:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa que crea Árboles Binarios de Búsqueda (ABB) a partir
de números que son ingresados por el usuario. Cabe mencionar que este Árbol puede
ser modificado, agregando o eliminando elementos (Números/Nodos) y se puede ser
representado de 4 formas distintas, 3 de ellas corresponden a las distintas formas
de recorrer un Árbol () y la cuarta, corresponde a una representación gráfica del
Árbol a través de la herramienta graphviz.


## Funcionamiento e Interacción:

- El programa comienza mostrando el menú del mismo en el que se pueden realizar
6 acciones. La primera consiste en agregar nodos al Árbol (Si es el primero el que
se ingresa, este será la raíz y, sino, será un nodo), luego de esta realizar esta
acción al usuario se le redigirá al menú principal. La segunda se encarga de
eliminar un nodo, para ello se le pregunta al usuario cual es el número que desea
eliminar; luego de realizada esta acción se redigirá automáticamente al menú principal.
La tercera consiste en la modificación de un nodo y, para ello, lo que se realiza
es la eliminación del nodo a modificar y su posterior reemplazo por un nodo nuevo.
La cuarta opción se encarga de mostrar el contenido del Árbol en los distintos tipos
de recorrido: Preorden, inorden y posorden; para ello se redirige al usuario a un
nuevo menú, en el cual, aparecerán 4 opciones: "Recorrido Preorden", "Recorrido Inorden", "Recorrido Posorden" y "Volver al menú principal"; cada vez que se escoja alguna de las
opciones de visualización, se le solicitará al usuario el ingreso de cualquier número
para volver al menú de las visualizaciones. La quinta opcioón del menú principal es
la que se encarga de generar el gráfico correspondiente al Árbol que se haya creado,
esto gracias a la acción de la herramienta "graphviz"; cabe mencionar que para ello,
en primer lugar, se crea un archivo en el cual se almacenan los nodos y, posterior a
esto, se procede a crear la imagen del grafo correspondiente que se visualizará en
pantalla. Por último, la sexta acción correspende al cierre del programa.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Cuando se quiere eliminar un nodo que no existe, el código se rompe automáticamente
y se da cierre al mismo.
- Cuando se quiere modificar un nodo, existe un error que no permite realizar esta
acción. El nodo selecionado es eliminado, pero no se puede agregar un nuevo nodo
para reemplazarlo.
- Al momento de ingresar un caracter de tipo "string" en el menú del tipo de
recorrido del Árbol, el código se romperá iniciando un ciclo infinito.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en la guía.
- Se crea un Árbol binario de búsqueda que solo contiene número enteros.
- Posee elementos únicos.
- Se pueden realziar operaciones básicas (Inserción, eliminación y modificación de
un número, visualización del Árbol en Preorden, Inorden y Posorden y generación
del grafo correspondiente.
- Programa no recibe parámetros de entrada.
- Para que el archivo y la imagen se creen correctamente, primero se debe instalar
el paquete de la herramienta "graphviz" con el siguiente comando:
"sudo apt-get install graphviz".
- El archivo que almacena los datos del Árbol generado se realiza con:
"dot -Tpng -ografo.png datos.txt".
- La imagen del grafo correspondiente al Árbol generado se realiza con:
"eog grafo.png".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa".
