#include <iostream>
#include "Nodo.h"

#ifndef ABB_H
#define ABB_H


class Arbol{

    private:

    public:
        // Constructor de la clase.
        Arbol();
        Nodo* crear_nodo(int numero, Nodo *progenitor);
        Nodo* encontrar_por_izquierda(Nodo *raiz);

        // Métodos de la clase.
        void destruir_nodo(Nodo *nodo);
        void agregar(Nodo *&raiz, int numero, Nodo *progenitor);
        void reemplazar_hijo(Nodo *raiz, Nodo *cambio);
        void eliminar_nodo(Nodo *&nodo_eliminado);
        void eliminacion(Nodo *&raiz, int numero);
        void imprimir_preorden(Nodo *raiz);
        void imprimir_inorden(Nodo *raiz);
        void imprimir_posorden(Nodo *raiz);
};
#endif
