#include <iostream>

using namespace std;


// Definición de la estrucutura de "Nodo".
typedef struct _Nodo{

    int dato;
    struct _Nodo *izq = NULL;
    struct _Nodo *der = NULL;
    struct _Nodo *nodo_progenitor = NULL;

} Nodo;
