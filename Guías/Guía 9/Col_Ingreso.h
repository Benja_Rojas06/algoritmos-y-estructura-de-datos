#include <iostream>

using namespace std;

#ifndef COL_INGRESO_H
#define COL_INGRESO_H


// Creación de la clase "Colisiones Ingreso".
class Col_Ingreso{

    // No hay atributos privados.
    private:

    // Atributos públicos.
    public:
        // Constructor por defecto.
        Col_Ingreso();

        // Método de impresión del arreglo.
        void imprimir_arreglo(int *array, int tamanio);

        // Métodos de búsqueda.
        // Prueba Lineal.
        void Col_PruebaLineal(int *array, int tamanio, int numero, int pos_actual);
        // Prueba Cuadrática.
        void Col_PruebaCuadratica(int *array, int tamanio, int numero, int pos_actual);
        // Doble Dirección Hash.
        void Col_DDHas(int *array, int tamanio, int numero, int pos_actual);
};
#endif
