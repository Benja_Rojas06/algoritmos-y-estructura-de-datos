#include <iostream>
#include "Col_Ingreso.h"

using namespace std;


// Definición del Constructor por defecto de la clase que buscará las
// colisiones de ingreso.
Col_Ingreso::Col_Ingreso(){}


// Función encargada de imprimir el arreglo en caso de que corresponda.
void Col_Ingreso::imprimir_arreglo(int *array, int tamanio){

    // Ciclo que va desde 0 hasta el tamaño del arreglo para recorrerlo
    // por completo.
    for(int i = 0; i < tamanio; i++){
        // Se imprime el arreglo posición por posición.
        cout << i << ".- [ " << array[i] << " ]" << endl;
    }
}


// Función con la cual se realizará el ingreso a través del método
// de Prueba Lineal.
void Col_Ingreso::Col_PruebaLineal(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;
    // Creación de un contador para que no se supere el tamaño del arreglo creado.
    int contador;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha ingresado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // La posición nueva es la actual más 1.
        pos_nueva = pos_actual + 1;
        // Contador se inicializa en 0.
        contador = 0;

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y la
        // posición nueva sea menor o igual al tamaño del arreglo y la posición
        // nueva sea distinta de la posición actual y el arreglo en la posición
        // nueva sea distinta al número ingresado.
        while((array[pos_nueva] != -1) && (pos_nueva <= tamanio) && (pos_nueva != pos_actual) && (array[pos_nueva] != numero)){

            // Se le suma 1 a la posición nueva.
            pos_nueva = pos_nueva + 1;

            // Si la posición nueva es igual al tamaño del arreglo más 1.
            if(pos_nueva == tamanio + 1){
                // Se "resetea" (Vuelve al incio) el valor de la posición actual
                // al salir del rango del arreglo.
                pos_nueva = 0;
            }
            // Se suma 1 al contador.
            contador = contador + 1;
        }

        // Si el contador posee el mismo tamaño del arreglo.
        if(contador == tamanio){
            // Se indica que no quedan más espacios.
            cout << "No quedan espacios en el arreglo para agregar más números." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }

        // Si el arreglo en la posición nueva es distinto de "vacío".
        if(array[pos_nueva] == -1){
            // Se agrega el número a la posición creada.
            array[pos_nueva] = numero;
            // Se indica la posición en la que se encuentra el número.
            cout << "El número '" << numero << "' ingresado, se movió a la ";
            cout << "posición '" << pos_nueva << "'." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }
    }
}


// Función con la cual se realizará el ingreso a través del método
// de Prueba Cuadrática.
void Col_Ingreso::Col_PruebaCuadratica(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;
    // Creación variable para realizar los incrementos en las posiciones.
    int num;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha ingresado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // Asignación del valor 1.
        num = 1;
        // La posición nueva es la actual más el cuadrado del número que
        // incrementa las posiciones.
        pos_nueva = (pos_actual + (num * num));

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y
        // el arreglo en la posición nueva sea distinto del número ingresado.
        while((array[pos_nueva] != -1) && (array[pos_nueva] != numero)){
            // Aumento en el número del incremento para avanzar en la posición.
            num = num + 1;
            // Generación de la posición nueva.
            pos_nueva = (pos_actual + (num * num));

            // Si la posición nueva es mayor que el tamaño del arreglo.
            if(pos_nueva > tamanio){
                // Número de incremento vuelve a 1.
                num = 1;
                // Posición nueva se resetea (Vuelve al incio).
                pos_nueva = 0;
                // Posición actual se resetea (Vuelve al incio).
                pos_actual = 0;
            }
        }

        // Si el arreglo en la posición nueva es distinto de "vacío".
        if(array[pos_nueva] == -1){
            // Se agrega el número a la posición creada.
            array[pos_actual] = numero;
            // Se indica la posición en la que se encuentra el número.
            cout << "El número '" << numero << "' ingresado, se movió a la ";
            cout << "posición '" << pos_nueva << "'." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }
    }
}


// Función con la cual se realizará el ingreso a través del método
// de Doble Dirección Hash.
void Col_Ingreso::Col_DDHas(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha ingresado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // Se indica el valor de la posición nueva.
        pos_nueva = ((pos_actual + 1)%tamanio - 1) + 1;

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y la
        // posición nueva sea menor o igual al tamaño del arreglo y la posición
        // nueva sea distinta de la posición actual y el arreglo en la posición
        // nueva sea distinta al número ingresado.
        while((array[pos_nueva] != -1) && (pos_nueva <= tamanio) && (pos_nueva != pos_actual) && (array[pos_nueva] != numero)){
            // Se actualiza el valor de la nueva posición.
            pos_nueva = ((pos_nueva + 1)%tamanio - 1) + 1;
        }

        // Si el array en la posición nueva es distinto de "vacío".
        if(array[pos_nueva] == -1){
            // Se agrega el número a la posición creada.
            array[pos_actual] = numero;

            // Si la posición nueva coincide con el tamaño del arreglo más 1.
            if(pos_nueva == tamanio + 1){
              // Se indica que no quedan más espacios.
              cout << "No quedan espacios en el arreglo para agregar más números." << endl;
            }

            // Sino
            else{
                // Se indica la posición en la que se encuentra el número.
                cout << "El número '" << numero << "' ingresado, se movió a la ";
                cout << "posición '" << pos_nueva << "'." << endl;
                // Se imprime el arreglo.
                cout << "\nSu arreglo es: " << endl << endl;
                imprimir_arreglo(array, tamanio);
            }
        }
    }
}
