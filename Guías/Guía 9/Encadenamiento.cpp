#include <iostream>
#include "Encadenamiento.h"

using namespace std;

// Definición del Constructor por defecto de la clase del método Encadenamiento.
Encadenamiento::Encadenamiento(){}


// Función encargada de copiar el array para realizar el método de Encadenamiento.
void Encadenamiento::copiado_array(int *array, Nodo **array1, int tamanio){

    // Ciclo con el cual se recorre el arreglo desde la posición 0 hasta la
    // final (Tamaño del arreglo).
    for(int i = 0; i < tamanio; i++){

        // "Arreglo copia" apunta al número para igualarlo al arreglo original
        // por cada posición iterada.
        array1[i]->numero = array[i];
        // Arreglo copia apunta al número siguiente y lo hace NULL.
        array1[i]->num_sig = NULL;
    }
}


// Función encargada de imprimir la lista enlazada generada.
void Encadenamiento::impresion_lista(Nodo **array, int pos_actual){

    // Definición de una variable auxiliar que es un nodo.
    Nodo *aux = NULL;
    // Variable auxiliar toma el valor del arreglo en la posición actual.
    aux = array[pos_actual];

    // Mientras el auxiliar sea distinto de NULL (Vacío).
    while(aux != NULL){

        // Si el auxiliar apuntando al número es distinto de 0.
        if(aux->numero != 0){

            // Se procede a imprimir la lista posición por posición.
            cout << "[ " << aux->numero << " ] - " << endl;
        }

        // Auxiliar se igual al auxiliar apuntando al número siguiente.
        aux = aux->num_sig;
    }
}


// Función encargada de realizar el método de colisiones por Encadenamiento.
void Encadenamiento::Col_Encadenamiento(int *array, int tamanio, int numero, int pos_actual){

    // Creación de un nodo temporal inicializado en NULL.
    Nodo *temp = NULL;
    // Creación de nuestro arreglo copia.
    Nodo *array1[tamanio];

    // Ciclo con el cual se recorre el arreglo desde la posición 0 hasta la
    // final (Tamaño del arreglo).
    for(int i = 0; i < tamanio; i++){
        // Se inicializa el nuestro nodo.
        array1[i] = new Nodo();
        // Se apunta al número siguiente y se define como NULL.
        array1[i]->num_sig = NULL;
        // Se apunta al número actual y se iguala a -1.
        array1[i]->numero = -1;
    }

    // Función encarga de copiar nuestro array.
    copiado_array(array, array1, tamanio);

    // Si el arreglo en la posición actual es distinto de -1 y el arreglo en la
    // posición actual es igual al número.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha buscado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual + 1 << "." << endl;
        // Se imprime la lista.
        cout << "\nSu lista enlazada es: " << endl <<endl;
        impresion_lista(array1, pos_actual);
    }

    // Sino
    else{
        // Nodo temporal se igual al arreglo copiado en la posición actual
        // apuntando al número siguiente.
        temp = array1[pos_actual]->num_sig;

        // Mientras el nodo temporal sea distinto de NULL y el nodo temporal
        // apuntando al número sea distinto del número.
        while((temp != NULL) && (temp->numero != numero)){
            // Nodo temporal avanza y apunta al siguiente número.
            temp = temp->num_sig;
        }

        // Si el nodo temporal es igual a NULL.
        if(temp == NULL){
            // Se indica que el número que se ha querido buscar no se encuentra en la lista.
            cout << "El número '" << numero << "' ingresado, no se encuentra en la lista." << endl;
            // Se imprime la lista.
            cout << "\nSu lista enlazada es: " << endl << endl;
            impresion_lista(array1, pos_actual);
        }

        // Sino
        else{
            // Se indica la posición del número que se ha buscado.
            cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual + 1 << ".\n" << endl;
            // Se imprime la lista.
            cout << "\nSu lista enlazada es: " << endl << endl;
            impresion_lista(array1, pos_actual);
        }
    }
}
