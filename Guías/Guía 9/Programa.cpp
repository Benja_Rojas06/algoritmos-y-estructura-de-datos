#include <iostream>
#include <string.h>
#include "Col_Ingreso.h"
#include "Col_Busqueda.h"
#include "Encadenamiento.h"


using namespace std;


// Función con la cual se imprimirá el arreglo.
void imprimir_arreglo(int *array, int tamanio){

    // Ciclo que recorre el arreglo desde la posición 0 hasta la última.
    for(int i = 0; i < tamanio; i++){
        // Se imprime el arreglo posición por posición gracias a la
        // iteración del ciclo.
        cout << i << ".- [ " << array[i] << " ]" << endl;
    }
}


// Función encargada de llenar el array creado con "-1" los cuales
// indicarán un espacio "vacío".
void llenado_arreglo(int *array, int tamanio){

    // Se recorre todo el arreglo gracias al ciclo.
    for(int i = 0; i < tamanio; i++){
        // A cada posición del arreglo se le asigna el valor "-1".
        array[i] = -1;
    }
}


// Función encargada de revisar el arreglo y de dar cuenta de la
// existencia o no de números.
bool revision_arreglo(int *array, int tamanio){

    // Variable contador para determinar la existencia de números en el arreglo.
    int contador;

    // Se recorre el arreglo desde la posición 0 hasta la final.
    for(int i = 0; i < tamanio; i++){
        // Si el arreglo en la posición que va iterando es distinto de -1.
        if(array[i] != -1){
            // Se le suma 1 al contador.
            contador = contador + 1;
        }
    }

    // Si no hay números en el areglo.
    if((contador > tamanio) && (contador < 0)){
        // Se retorna un false.
        return false;
    }

    // Sino, hay números en el arreglo.
    else{
        // Se retorna un true.
        return true;
    }
}


// Función para identificar las colisiones en la búsqueda de los números.
void col_busqueda(char *metodo, int *array, int tamanio, int numero, int pos_actual){

    // Llamado clase para las colisiones en la búsqueda.
    Col_Busqueda colisionBu = Col_Busqueda();

    // Si se encuentra una coincidencia con "L" (Prueba Lineal).
    if(strcmp(metodo, "L") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Lineal.
        colisionBu.Col_PruebaLineal(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "C" (Prueba Cuadrática).
    else if(strcmp(metodo, "C") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Cuadrática.
        colisionBu.Col_PruebaCuadratica(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "D" (Doble Dirección Hash).
    else if(strcmp(metodo, "D") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Doble Dirección Hash.
        colisionBu.Col_DDHas(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "E" (Encadenamiento).
    else if(strcmp(metodo, "E") == 0){
        Encadenamiento met_encade = Encadenamiento();
        met_encade.Col_Encadenamiento(array, tamanio, numero, pos_actual);
    }
}


// Función para identificar las colisiones al ingreso de los números.
void col_ingreso(char *metodo, int *array, int tamanio, int numero, int pos_actual){

    // Llamado clase para las colisiones en el ingreso.
    Col_Ingreso colisionIn = Col_Ingreso();

    // Si se encuentra una coincidencia con "L" (Prueba Lineal).
    if(strcmp(metodo, "L") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Lineal.
        colisionIn.Col_PruebaLineal(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "C" (Prueba Cuadrática).
    else if(strcmp(metodo, "C") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Cuadrática.
        colisionIn.Col_PruebaCuadratica(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "D" (Doble Dirección Hash).
    else if(strcmp(metodo, "D") == 0){
        // Llamado función respectiva al método de búsqueda Prueba Doble Dirección Hash.
        colisionIn.Col_DDHas(array, tamanio, numero, pos_actual);
    }

    // Si se encuentra una coincidencia con "E" (Encadenamiento).
    else if(strcmp(metodo, "E") == 0){
        Encadenamiento met_encade = Encadenamiento();
        met_encade.Col_Encadenamiento(array, tamanio, numero, pos_actual);
    }
}


// Función con la cual se determina la posición a ocupar por el número ingresado.
void func_hash(char *metodo, int *array, int tamanio, int numero){

    // Variable para indicar la posición actual del número.
    int pos_actual;

    // Cálculo de la posición en donde irá el número en base a este mismo.
    pos_actual = (numero%(tamanio - 1)) + 1;

    // Si existe una colisión.
    if(array[pos_actual] != -1){
        // Se indica donde ha ocurrido la colisión.
        cout << "Ha ocurrido una colisión en la posición '" << pos_actual << "'." << endl << endl;
        // Se procede a revisar la colisión mediante el método ingresado
        // al inicio del programa.
        col_ingreso(metodo, array, tamanio, numero, pos_actual);
    }

    // Si no
    else{
        // Se agrega el número a la posición creada.
        array[pos_actual] = numero;
        // Se procede a imprimir el arreglo con el número agregado.
        imprimir_arreglo(array, tamanio);
    }
}


// Función encargada de solicitar el número a ingresar o buscar.
int ingreso_numero(int tipo_accion){

    // Creación variable número que
    int numero;
    // Impresión del título del programa.
    cout << "\t.~Métodos de Búsqueda.~" << endl << endl;

    // Limpieza de la pantalla.
    system("clear");

    // Si se quiere agregar un número.
    if(tipo_accion == 1){
        // Se solicita el ingreso del número a agregar.
        cout << "Ingrese el número que desea agregar: ";
        // Se guarda en la variable.
        cin >> numero;
    }

    // Si se quiere buscar un número.
    else if(tipo_accion == 2){
        // Se solicita el ingreso del número a buscar.
        cout << "Ingrese el número que desea buscar: ";
        // Se guarda en la variable.
        cin >> numero;
    }

    // Limpieza de la pantalla.
    system("clear");

    cout << "\t.~Métodos de Búsqueda~." << endl << endl;

    // Retorno del número a agregar o buscar.
    return numero;
}


// Función que muestra las opciones que posee el menú.
string opc_menu(){

    // Creación de la variable acción que indicará la acción que se quiera realizar.
    string accion;

    // Limpieza de la pantalla.
    system("clear");
    // Opciones del menú.
    cout << "\t.~Métodos de Búsqueda~." << endl << endl;
    cout << "¿Que acción desea realizar sobre el arreglo?" << endl << endl;
    cout << "1.- Agregar número." << endl;
    cout << "2.- Buscar número." << endl;
    cout << "3.- Salir." << endl;

    // Se solicita la acción a realizar.
    cout << "\nIngrese la acción que desea realizar: ";
    // Se guarda en la variable acción.
    cin >> accion;

    // Retorno de la acción escogida.
    return accion;
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(char *metodo){

    // Número a agregar o buscar en el arreglo.
    int numero;
    // Posición actual del arreglo.
    int pos_actual;
    // Tamaño del arreglo.
    int tamanio;
    // Bandera con la cual se comprobará el estado del arreglo (Si este posee
    // números para buscar o no).
    bool estado_array;
    // Varible para realizar el retorno a las opciones del menú.
    string salir;

    // Limpieza de la pantalla.
    system("clear");
    cout << "\t.~Métodos de Búsqueda.~" << endl << endl;
    cout << "Ingrese el tamaño del tamaño del arreglo a generar: ";
    // Guardado de la variable del tamaño del arreglo.
    cin >> tamanio;

    // Creación del arreglo.
    int array[tamanio];

    // Llamado de la función encargada de rellenar el array creado con "-1".
    llenado_arreglo(array, tamanio);

    // Variable utilizada para la opción del menú escogida.
    string accion;

    // Mientras sea verdadero.
    while (true){

        // Se llamará a función menú para imprimir las opciones del menú.
        accion = opc_menu();

        // Se agregan números al arreglo.
        if(accion == "1"){
            // Se solicita el número a agregar.
            numero = ingreso_numero(1);
            // Llamado a la función para buscar una posición en la que se pueda
            // agregar el número.
            func_hash(metodo, array, tamanio, numero);

            // Retorno al menú presionando cualquier tecla.
            cout << "\nPresione cualquier tecla seguida de otra para volver al";
            cout << "menú principal (Ej: 'a b'): ";
            // Guardado de la variable previamente definida.
            cin >> salir;

            // Condicionales para volver al menú.
            // Si es igual al string 0
            if(salir == "0"){
                // Se retorna a las opciones del menú.
                opc_menu();
            }
            // O a cualquiera
            else{
                // Se retorna a las opciones del menú.
                opc_menu();
            }
        }

        // Se buscan números en el arreglo.
        else if(accion == "2"){

            // Se revisa si es que el arreglo ha sido creado (Si posee números).
            estado_array = revision_arreglo(array, tamanio);

            cout << "El estado del arreglo es: " << estado_array << endl;

            // Si existen números en el arreglo.
            if(estado_array == true){
                // Se solicita el número a buscar.
                numero = ingreso_numero(2);

                // Se le da un valor a la posición actual.
                pos_actual = (numero%(tamanio - 1)) + 1;

                // Limpieza de pantalla.
                system("clear");

                // Si el arreglo en la posición actual es igual al número buscado.
                if(array[pos_actual] == numero){
                    // Se indica la posición del número que se ha buscado.
                    cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
                    // Se imprime el arreglo.
                    cout << "\n" << endl;
                    imprimir_arreglo(array, tamanio);
                }
                // Sino
                else{
                    // Se indica donde ha ocurrido la colisión.
                    cout << "Ha ocurrido una colisión en la posición '" << pos_actual << "'." << endl << endl;
                    // Se procede a revisar la colisión mediante el método ingresado
                    // al inicio del programa.
                    col_busqueda(metodo, array, tamanio, numero, pos_actual);
                }
            }
            // Sino
            else{
                // Limpieza de pantalla.
                system("clear");
                // Se indica que el arreglo se encuentra vacío.
                cout << "Aún no ha ingresado ningún número al arreglo." << endl;
            }

            // Retorno al menú presionando cualquier tecla.
            cout << "\nPresione cualquier tecla seguida de otra para volver al";
            cout << "menú principal (Ej: 'a b'): ";
            // Guardado de la variable previamente definida.
            cin >> salir;

            // Condicionales para volver al menú.
            // Si es igual al string 0
            if(salir == "0"){
                // Se retorna a las opciones del menú.
                opc_menu();
            }
            // O a cualquiera
            else{
                // Se retorna a las opciones del menú.
                opc_menu();
            }
        }


        // Se cierra el programa.
        else if(accion == "3"){
            // Limpieza de la pantalla.
            system("clear");
            // Mensaje de despedida.
            cout << "Que tenga un buen día..." << endl;
            // Pantalla se queda suspendida 1 segundo.
            system("sleep 1");
            // Se cierra el programa.
            exit(0);
            // Se rompre el ciclo.
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            // Limpieza de la pantalla.
            system("clear");
            // Se indica que se ingrese una opción válida.
            cout << "Favor, ingrese una acción válida." << endl;
            // Suspensión de pantalla por 1,5 segundos.
            system("sleep 1.5");
            // Llamado a la función para realizar las opciones del menú.
            opc_menu();
        }
    }
}


// Función principal con la cual se realizan la validación del parámetro
// búsqueda a realizar.
int main(int argc, char **argv){

    // Variable para el método de búsqueda a realizar.
    string metodo;

    // Se indica que se debe ingresar un parámetro.
    if(argc != 2){
        // Indicaciones de como se debe ingresar el párametro.
        cout << "\nPor favor, para iniciar el programa ingrese 1 parámetro." << endl;
        cout << "Este corresponde al método de búsqueda que quiera realizar y " << endl;
        cout << "puede ser L, C, D o E." << endl << endl;
        cout << "Ejemplo de ejecución: './Programa L'" << endl << endl;
    }

    // Sino
    else{
        // método toma la posición 1.
        metodo = argv[1];

        // Si el método de búsqueda es distinto de L y de C y de D y de E.
        if((metodo != "L") && (metodo != "C") && (metodo != "D") && (metodo != "E")){
            // Se indica que el parámetro es inválido.
            cout << "\nEl método ingresado para la búsqueda no es válido." << endl;
            cout << "Debe ingresar 'L' (Prueba Lineal), 'C' (Prueba Cuadrática), ";
            cout << "'D' (Doble Dirección Hash) o 'E' (Encadenamiento)." << endl << endl;
            // Cierre del programa.
            exit(1);
        }

        // Llamado de la función menú para dar inicio al programa.
        menu(argv[1]);
    }
    // Retorno de 0;
    return 0;
}
