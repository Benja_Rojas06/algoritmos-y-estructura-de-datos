# Propuesta solución Guía 9:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa que realiza dos métodos de búsqueda. Estos son Método
de Prueba Lineal, de Prueba Cuadrática, de Doble Dirección Hash y Encadenamiento.
Esto gracias a un arreglo que se llena con "-1" y al cual solo debe entregarse el
tamaño del mismo. Posteriormente, se pueden realizar los métodos de búsqueda para
colisiones en el ingreso o en la búsqueda como tal.  


## Funcionamiento e Interacción:

- El programa se inicializa ingresando un parámetro, el cual corresponde al tipo
de método que se quiera llevar a cabo (Este puede ser "L" para Prueba Lineal, "C"
para Prueba Cuadrática, "D" para Doble Dirección Hash y "E" para Encadenamiento),
luego de ingresar el comando "./Programa" en la terminal, con el cual se ejecuta
el mismo. Un ejemplo es: "./Programa L". De este modo se le indica al programa
que el método de búsqueda a realizar es el de Prueba Lineal. Posterior a ello se
procederá a solicitar al usuario que ingrese el largo del arreglo a generar, el
cual, y como ya se dijo anteriormente, se inicializará con todas las posiciones
siendo "-1". Una vez ingresado el largo, se desplegará un menú con 3 opciones; la
primera de ellas es para agregar un número al arreglo, posterior a ello se
procederá a solicitar el número a ingresar, se imprimirá el arreglo que se tiene
hasta el momento y se indicará que para volver al menú principal, el usuario puede
presionar cualquier tecla. Cabe mencionar que del segundo ingreso en adelante se
indicarán las colisiones y cambios de posiciones cuando estos existan; la segunda
opción consiste en realizar las búsquedas, a la cual, luego de ingresar, se
solicitará el número a buscar. En este punto tenemos dos opciones, que el número
no se encuentre en el arreglo o que sí se encuentre en el arreglo, si el caso es
el primero se indicará que el número no se encuentra en el arreglo y se procederá
a imprimir el mismo, si el caso es el segundo, se indicará que si se encuentra en
el arreglo, además de la posición en la que lo hace y, seguidamente, se imprimirá
el arreglo en cuestión; por último, la tercera opción se utiliza para cerrar el
programa.
Cabe mencionar que si se ingresa cualquier otra cosa, el programa indicará que se
ha ingresado una opción inválida y se retornará al menú principal.


## Fallos que posee el programa:

- Si se ingresa una variable de tipo string cuando se soliciten los números a
ingresar o buscar, el programa se romperá por completo y se tendrá que forzar su
cierre.
- Cada vez que se realiza un retorno al menú principal, tanto cuando se ingresa
una opción inválida en el inicio, como cuando se retorna luego de agregar o buscar
un número. Se debe ingresar primero cualquier cosa y luego la acción a realizar.
Un ejemplo de ello es: "Retorno al menú principal", se ingresa una "a" y luego
"1", "2" o "3" según la acción que se desee realizar. En su defecto, se puede
ingresar cualquier cosa, luego un espacio y después cualquier cosa para que así
el menú funcione "bien" sin tener que realizar un ingreso doble. Esto del
siguiente modo: "a b". Esto genera que no se deba realizar el ingreso de
"cualquier cosa" anteriormente descrita.
- Método de Doble Dirección Hash presenta fallos al ingresar los números, ya que
si se presenta una colisión, se indica que esta a ocurrido, además de la posición,
se imprime que el número se ha movido de posición, pero en la práctica esto no
ocurre porque el número agregado reemplaza al número con el cual tuvo la colisión.
- Método de Encadenamiento se rompe con facilidad al ingresar los números.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases.
- Se imprime el contenido del arreglo y lista enlazada si corresponde por cada
ingreso.
- Si existe una colisión se indica dónde fue y cual fue el desplazamiento final
para cada ingreso y búsqueda.
- Por cada colisión, se indica la posición en la que esta ha ocurrido.
- Se debe realizar la instalación de "make" para que no ocurra ningún problema
en la compilación, esto con "sudo apt install make".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa" seguido del parámetro.
Ej: "./Programa L".
