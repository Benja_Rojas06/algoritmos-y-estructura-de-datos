#include <iostream>

using namespace std;

#ifndef COL_BUSQUEDA_H
#define COL_BUSQUEDA_H


// Creación de la clase "Colisiones Búsqueda".
class Col_Busqueda{

    // No hay atributos privados.
    private:

    // Atributos públicos.
    public:
        // Constructor por defecto.
        Col_Busqueda();

        // Método de impresión del arreglo.
        void imprimir_arreglo(int *array, int tamanio);

        // Métodos de búsqueda.
        // Prueba Lineal.
        void Col_PruebaLineal(int *array, int tamanio, int numero, int pos_actual);
        // Prueba Cuadrática.
        void Col_PruebaCuadratica(int *array, int tamanio, int numero, int pos_actual);
        // Doble Dirección Hash.
        void Col_DDHas(int *array, int tamanio, int numero, int pos_actual);
};
#endif
