#include <iostream>

using namespace std;


// Definición de la estrucutura de "Nodo".
typedef struct _Nodo{

    // Variable correspondiente al número ingresado.
    int numero;
    // Creación del nodo que apunta al número siguiente.
    struct _Nodo *num_sig;

} Nodo;


// Creación de la clase "Encadenamiento".
class Encadenamiento{

    // No hay atributos privados.
    private:

    // Atributos públicos.
    public:
        // Constructor por defecto.
        Encadenamiento();

        // Método de copiado del arreglo para realizar el método de encadenamiento.
        void copiado_array(int *array, Nodo **array1, int tamanio);
        // Método de impresión de la lista generada.
        void impresion_lista(Nodo **array, int pos_actual);
        // Método de Búsqueda por Encadenamiento.
        void Col_Encadenamiento(int *array, int tamanio, int numero, int pos_actual);
};
