#include <iostream>
#include "Col_Busqueda.h"

using namespace std;


// Definición del Constructor por defecto de la clase que buscará las
// colisiones de búsquda.
Col_Busqueda::Col_Busqueda(){}


// Función encargada de imprimir el arreglo en caso de que corresponda.
void Col_Busqueda::imprimir_arreglo(int *array, int tamanio){

    // Ciclo que va desde 0 hasta el tamaño del arreglo para recorrerlo
    // por completo.
    for(int i = 0; i < tamanio; i++){
        // Se imprime el arreglo posición por posición.
        cout << i << ".- [ " << array[i] << " ]" << endl;
    }
}


// Función con la cual se realizará la búsqueda a través del método
// de Prueba Lineal.
void Col_Busqueda::Col_PruebaLineal(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha buscado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // La posición nueva es la actual más 1.
        pos_nueva = pos_actual + 1;

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y la
        // posición nueva sea menor o igual al tamaño del arreglo y la posición
        // nueva sea distinta de la posición actual y el arreglo en la posición
        // nueva sea distinta al número ingresado.
        while((array[pos_nueva] != -1) && (pos_nueva <= tamanio) && (pos_nueva != pos_actual) && (array[pos_nueva] != numero)){

            // Se le suma 1 a la posición nueva.
            pos_nueva = pos_nueva + 1;

            // Si la posición nueva es igual al tamaño del arreglo más 1.
            if(pos_nueva == tamanio + 1){
                // Se "resetea" (Vuelve al incio) el valor de la posición actual
                // al salir del rango del arreglo.
                pos_nueva = 0;
            }
        }

        // Si el arreglo en la posición nueva es distinto de "vacío" y la posición
        // nueva es igual a la posición actual.
        if((array[pos_nueva] == -1) || (pos_nueva == pos_actual)){
            // Se indica que el número que se ha querido buscar no se encuentra en el arreglo.
            cout << "El número '" << numero << "' ingresado, no se encuentra en el arreglo." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }

        // Sino
        else{
            // Se indica la posición del número que se ha buscado.
            cout << "El número '" << numero << "' ingresado, está en la posición " << pos_nueva << ".\n" << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }
    }
}


// Función con la cual se realizará la búsqueda a través del método
// de Prueba Cuadrática.
void Col_Busqueda::Col_PruebaCuadratica(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;
    // Creación variable para realizar los incrementos en las posiciones.
    int num;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha buscado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // Asignación del valor 1.
        num = 1;
        // La posición nueva es la actual más el cuadrado del número que
        // incrementa las posiciones.
        pos_nueva = (pos_actual + (num * num));

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y
        // el arreglo en la posición nueva sea distinto del número ingresado.
        while((array[pos_nueva] != -1) && (array[pos_nueva] != numero)){
            // Aumento en el número del incremento para avanzar en la posición.
            num = num + 1;
            // Generación de la posición nueva.
            pos_nueva = (pos_actual + (num * num));

            // Si la posición nueva es mayor que el tamaño del arreglo.
            if(pos_nueva > tamanio){
                // Número de incremento vuelve a 1.
                num = 1;
                // Posición nueva se resetea (Vuelve al incio).
                pos_nueva = 0;
                // Posición actual se resetea (Vuelve al incio).
                pos_actual = 0;
            }
        }

        // Si el arreglo en la posición nueva es distinto de "vacío".
        if(array[pos_nueva] == -1){
            // Se indica que el número que se ha querido buscar no se encuentra en el arreglo.
            cout << "El número '" << numero << "' ingresado, no se encuentra en el arreglo." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }

        // Sino
        else{
            // Se indica la posición del número que se ha buscado.
            cout << "El número '" << numero << "' ingresado, está en la posición " << pos_nueva << "." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }
    }
}


// Función con la cual se realizará la búsqueda a través del método
// de Doble Dirección Hash.
void Col_Busqueda::Col_DDHas(int *array, int tamanio, int numero, int pos_actual){

    // Creación variable correspondiente a la posición nueva.
    int pos_nueva;

    // Si el arreglo en la posición actual es distinto de "vacío" y el arreglo en
    // en la posición actual es igual al número ingresado.
    if((array[pos_actual] != -1) && (array[pos_actual] == numero)){
        // Se indica la posición del número que se ha buscado.
        cout << "El número '" << numero << "' ingresado, está en la posición " << pos_actual << "." << endl;
        // Se imprime el arreglo.
        cout << "\nSu arreglo es: " << endl << endl;
        imprimir_arreglo(array, tamanio);
    }

    // Sino
    else{
        // Se indica el valor de la posición nueva.
        pos_nueva = ((pos_actual + 1)%tamanio - 1) + 1;

        // Mientras el arreglo en la posición nueva sea distinto de "vacío" y la
        // posición nueva sea menor o igual al tamaño del arreglo y la posición
        // nueva sea distinta de la posición actual y el arreglo en la posición
        // nueva sea distinta al número ingresado.
        while((array[pos_nueva] != -1) && (pos_nueva <= tamanio) && (pos_nueva != pos_actual) && (array[pos_nueva] != numero)){
            // Se actualiza el valor de la nueva posición.
            pos_nueva = ((pos_nueva + 1)%tamanio - 1) + 1;
        }

        // Si el array en la posición nueva es distinto de "vacío" o la posición
        // nueva es igual a la posición actual.
        if((array[pos_nueva] == -1) || (pos_nueva == pos_actual)){
            // Se indica que el número que se ha querido buscar no se encuentra en el arreglo.
            cout << "El número '" << numero << "' ingresado, no se encuentra en el arreglo." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }

        // Sino
        else{
            // Se indica la posición del número que se ha buscado.
            cout << "El número '" << numero << "' ingresado, está en la posición " << pos_nueva << "." << endl;
            // Se imprime el arreglo.
            cout << "\nSu arreglo es: " << endl << endl;
            imprimir_arreglo(array, tamanio);
        }
    }
}
