# Propuesta solución Guía 5:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa que crea Árboles AVL a partir de ID's que son ingresados
por el usuario. Cabe mencionar que este Árbol puede ser modificado, agregando o
eliminando elementos (ID's/Nodos) y se puede ser representado de forma gráfica
gracias a la herramienta graphviz.


## Funcionamiento e Interacción:

- El programa comienza mostrando el menú del mismo en el que se pueden realizar
5 acciones. La primera consiste en agregar nodos al Árbol (Si es el primero el que
se ingresa, este será la raíz y, sino, será un nodo), luego de realizar esta
acción al usuario se le indicará que el ID ha sido agregado correctamente y se le
redigirá al menú principal. La segunda se encarga de eliminar un nodo, para ello
se le pregunta al usuario cual es el número que desea eliminar, esto no sin antes
imprimir en pantalla los ID's ingresados hasta el momento (Esto en recorrido
Inorden); luego de realizada esta acción se le indicará al usuario si el ID ingresado
se ha eliminado con éxito (El ID sí existe dentro del Árbol) o no (ID no existe
en el Árbol) y se redigirá automáticamente al menú principal. La tercera consiste
en la modificación de un nodo y, para ello, lo que se realiza, primeramente, es
mostrar en pantalla los ID que se han ingresado hasta el momento (En recorrido
Inorden), luego de esto se le solicita al usuario que ingrese cúal es el ID que
desea eliminar, si este ID es encontrado en el Árbol, se procede a eliminar y, a
continuación, se solicita el ingreso del ID que reemplazará al eliminado, si no
existe el ID ingresado, se le informará de esto al usuario y será redirigido al
menú principal. La cuarta opción del menú principal es la que se encarga de generar
el gráfico correspondiente al Árbol que se haya creado, esto gracias a la acción
de la herramienta "graphviz"; cabe mencionar que para ello, en primer lugar, se
crea un archivo en el cual se almacenan los nodos y, posterior a esto, se procede
a crear la imagen del grafo correspondiente que se visualizará en pantalla.
Por último, la quinta acción correspende al cierre del programa.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Cuando se quiere modificar un nodo, existe un error al momento de ingresar un
ID que aún no ha sido ingresado, puesto que se indica que ID no existe, pero, sin
embargo, posterior a este mensaje se solicita el ingreso del ID a agregar (Se pobró
cambiando las condicionales de los retornos, pero a la larga era peor porque solo
dejaba modificar un ID; decia que se había encontrado el ID, pero luego se redirigía
al menú principal, como cuando ocurre para el caso de los ID que no existen).
- Si se ingresan los ID's muy rápido, el código es propenso a romperse con el fallo
"Segmentation fault".
- No se implementa el cargado de archivo puesto que no se ha podido generar el
gráfico a partir de los archivos dados por el profesor.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en la guía.
- Se crea un Árbol AVL que contiene variables de tipo string (ID).
- Posee elementos únicos.
- Se pueden realizar operaciones básicas (Inserción, eliminación y modificación de
un ID y generación del grafo correspondiente).
- Programa no recibe parámetros de entrada.
- Para que el archivo y la imagen se creen correctamente, primero se debe instalar
el paquete de la herramienta "graphviz" con el siguiente comando:
"sudo apt-get install graphviz".
- El archivo que almacena los datos del Árbol generado se realiza con:
"dot -Tpng -ografo.png datos.txt".
- La imagen del grafo correspondiente al Árbol generado se realiza con:
"eog grafo.png".
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa".
