#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "AVL.h"

using namespace std;


// Clase "Grafo" con la cual se recorrerá el Árbol y se
// creará su grafo correspondiente.
class Grafo{

    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la clase Grafo.
        Grafo(Nodo *raiz){
            this->arbol = raiz;
        }
    // Recorrido del Árbol.
    void recorrer_grafo(Nodo *p, ofstream &archivo){

      string cadena = "\0";

      // Enlazamiento de los nodos al grafo. Para generar una diferencia entre
      // izquierda y derecha, a cada nodo creado se le entrega un identificador.
      if(p != NULL){

          // Enlazamiento por izquierda.
          if(p->izq != NULL){
              // Se escribe el nodo dentro del archivo.
              archivo << "\"" << p->id << "\"->\"" << p->izq->id;
              archivo << "\"[label=\"" << p->izq->factor_E << "\"]" << endl;
          }

          else{
              cadena = p->id + "i";
              archivo << "\"" << cadena << "\" [shape=point]; " << endl;
              archivo << "\"" << p->id << "\"->" << "\"" << cadena << "\";" << endl;
          }

          // Enlazamiento por derecha.
          if(p->der != NULL){
              // Se escribe el nodo dentro del archivo.
              archivo << "\"" << p->id << "\"->\"" << p->der->id;
              archivo << "\"[label=\"" << p->der->factor_E << "\"];" << endl;
          }

          else{
              cadena = p->id + "d";
              archivo << "\"" << cadena << "\" [shape=point]; " << endl;
              archivo << "\"" << p->id << "\"->" << "\"" << cadena << "\";" << endl;
          }

          // Llamado para el recorrido del Árbol por izquierda y por derecha
          // para la creación del grafo.
          recorrer_grafo(p->izq, archivo);
          recorrer_grafo(p->der, archivo);
      }
      return;
    }

    // Creación del grafo.
    // ofstream es el tipo de dato que corresponde a archivos en cpp.
    void generar_grafo(Nodo *p){

        ofstream archivo;

        // Creación (Apertura) del archivo "datos.txt" con el cual se
        // generará el grafo.
        archivo.open("datos.txt");
        // Dentro del archivo creado se escribe "digraph G {".
        archivo << "digraph G {\n";
        // Se escoge un color a modo de personalizar los nodos.
        archivo << "node [style=filled fillcolor=green];\n";

        archivo << "nullraiz[shape=point]" << endl;
        archivo << "nullraiz->\"" << p->id << "\" [label=" << p-> factor_E << "];" << endl;

        // Se llama a la función "recorrer_grafo" para que genere el archivo
        // de texto y, así, poder generar el grafo.
        recorrer_grafo(this->arbol, archivo);

        // Impresión de "}" para denotar que ya no se escribirá más.
        archivo << "}" << endl;
        // Cierre del archivo.
        archivo.close();

        // Generación del grafo.
        system("dot -Tpng -ografo.png datos.txt &");
        // Visualización del grafo.
        system("eog grafo.png &");
    }
};


// Función encargada de solicitar el ID a ingresar.
string ingreso_id(int tipo_accion){

    string id;

    // Si se quiere agregar un ID.
    if(tipo_accion == 1){
        // Se solicita el ingreso del id a agregar.
        cout << "\nIngrese el ID que desea agregar: ";
        getline(cin, id);
    }

    // Si se quiere eliminar un ID.
    else if(tipo_accion == 2){
        // Se solicita el ingreso del ID a eliminar.
        cout << "\nIngrese el ID que desea eliminar: ";
        getline(cin, id);
    }

    // Si se quiere modificar un ID.
    else if(tipo_accion == 3){
        // Se solicita el ingreso del ID a modificar.
        cout << "\nIngrese el ID que desea modificar: ";
        getline(cin, id);
    }

    // Retorno del ID a agregar, eliminar o modificar.
    return id;
}


// Función que muestra las opciones que posee el menú.
int opc_menu(){

    string accion;

    // Opciones del menú.
    system("clear");
    cout << "\t.~Bienvenido al generador de AVL~." << endl << endl;
    cout << "¿Que acción desea realizar sobre el Árbol?" << endl << endl;
    cout << "1.- Agregar nodo (ID)." << endl;
    cout << "2.- Eliminar nodo." << endl;
    cout << "3.- Modificar nodo." << endl;
    cout << "4.- Generar el grafo." << endl;
    cout << "5.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    getline(cin, accion);

    // Se retorna la acción ingresada.
    return stoi(accion);
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(Nodo *raiz, Arbol *arbol){

    int accion;
    bool altura = false;
    string identificador;
    string identificador2;

    while (true){

        // Llamado función menú para imprimir las opciones del menú.
        accion = opc_menu();

        // Se agregan los nodos (ID's) al Árbol.
        if(accion == 1){
            system("clear");
            // Se solicita el nodo (ID) a agregar.
            identificador = ingreso_id(1);
            // Creación del nodo a partir del ID ingresado.
            arbol->agregar_nodo(raiz, identificador, altura);
        }

        // Se eliminan los nodos (ID's) del Árbol.
        else if(accion == 2){
            system("clear");
            // Se imprimen los ID's (En Inorden) que se tienen hasta el momento
            // para que el usuario sepa cuales puede eliminar.
            cout << "\t.~ID's ingresados hasta el momento~." << endl << endl;
            arbol->imprimir_inorden(raiz);
            cout << endl;

            // Se solicita el nodo (ID) a eliminar.
            identificador = ingreso_id(2);
            // Eliminación de un nodo (ID) previamente seleccionado.
            arbol->eliminar_nodo(raiz, identificador, altura);
        }

        // Se modifican los nodos (ID's) del Árbol. Para ello, se elimina el
        // nodo y, posteriormente, se agrega uno nuevo en la misma ubicación.
        else if(accion == 3){
            system("clear");

            // Si la raíz existe.
            if(raiz != NULL){
                // Se imprimen los ID's (En Inorden) que se tienen hasta el momento
                // para que el usuario sepa cuales puede eliminar.
                cout << "\t.~ID's ingresados hasta el momento~." << endl << endl;
                arbol->imprimir_inorden(raiz);
                cout << endl;

                // Se solicita el ID a modificar.
                identificador = ingreso_id(3);

                // Si encuentra el ID ingresado, se procede a modificar.
                if(arbol->busqueda_nodo(raiz, identificador) == true){
                    // Llamado función encargada de eliminar el nodo (ID) ingresado.
                    arbol->eliminar_nodo(raiz, identificador, altura);
                    // Se solicita el ingreso del ID a agregar.
                    identificador2 = ingreso_id(1);
                    // Llamado función encargada de agregar el nodo (ID) ingresado.
                    arbol->agregar_nodo(raiz, identificador2, altura);
                }
                // Si no se encuentra el ID ingresado.
                else if(arbol->busqueda_nodo(raiz, identificador) == false){
                    // Se le informa al usuario que será redirigido al menú principal.
                    cout << "Será redirigido al menú principal..." << endl;
                    system("sleep 2");
                }
            }
            // En cambio, si no existe, no se puede modificar nada.
            else{
                cout << "El Árbol se encuentra vacío, por lo que no puede modificar." << endl;
                system("sleep 2.3");
            }
        }

        // Se genera el grafo correspondiente al Árbol.
        else if(accion == 4){
            system("clear");
            // Representación gráfica (Grafo) correspondiente al Árbol.
            Grafo *grafo = new Grafo(raiz);
            grafo->generar_grafo(raiz);
        }

        // Cierre del programa.
        else if(accion == 5){
            // Mensaje de despedida.
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            // Se cierra el programa.
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            opc_menu();
        }
    }
}


// Función principal que define la raíz y el árbol nuevo. Además, se encarga de
// llamar a la función "menú" para el "inicio" del programa.
int main(){

    // Definición del árbol nuevo.
    Arbol *arbol = new Arbol();

    // Definición de la raíz.
    Nodo *raiz = NULL;

    // Llamado a la función menú.
    menu(raiz, arbol);

    return 0;
}
