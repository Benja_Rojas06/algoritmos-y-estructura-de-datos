#include <iostream>

using namespace std;


// Definición de la estrucutura de "Nodo".
typedef struct _Nodo{

    // Creación de la variable id (ID).
    string id;

    // Creación del Factor de Equilibrio.
    // Es la sustracción entre el subárbol derecho e izquierdo.
    int factor_E;

    // Creación de los nodos de izquierda, derecha y el progenitor.
    struct _Nodo *izq = NULL;
    struct _Nodo *der = NULL;

} Nodo;
