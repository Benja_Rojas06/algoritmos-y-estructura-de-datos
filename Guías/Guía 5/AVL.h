#include <iostream>
#include "Nodo.h"

#ifndef AVL_H
#define AVL_H

class Arbol{

    private:
      
    public:
        // Constructor del Árbol.
        Arbol();

        // Creación del nodo.
        Nodo* crear_nodo(string identificador);

        // Inserción del nodo.
        void agregar_nodo(Nodo *&raiz, string id_a_agregar, bool &altura);

        // Eliminación del nodo.
        void eliminar_nodo(Nodo *&raiz, string id_a_agregar, bool &altura);

        // Búsqueda de un nodo.
        bool busqueda_nodo(Nodo *raiz, string id_a_agregar);

        // Impresión inorden del Árbol.
        void imprimir_inorden(Nodo *raiz);

        // Reestructuración por izquierda.
        void reestructuracion_por_izq(Nodo *&raiz, bool altura);
        // Rotación de las ramas derechas.
        void rotacion_DD(Nodo *&raiz, bool altura, Nodo *nodo1, int tipo_rotacion);
        // Rotación de las ramas derechas y luego las izquierdas.
        void rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2, int tipo_rotacion);

        // Reestructuración por derecha.
        void reestructuracion_por_der(Nodo *&raiz, bool altura);
        // Rotación de las ramas izquierdas.
        void rotacion_II(Nodo *&raiz, bool altura, Nodo *nodo1, int tipo_rotacion);
        // Rotación de las ramas izquierdas y luego las derechas.
        void rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2, int tipo_rotacion);
};
#endif
