#include <iostream>
#include "AVL.h"

using namespace std;


// Constructor por defecto de la clase 'Árbol'.
Arbol::Arbol(){}


// Creación de los nodos.
Nodo* Arbol::crear_nodo(string identificador){

    // Creación del nodo.
    Nodo *temp;
    temp = new Nodo;

    // Asignación de un valor al nodo creado.
    temp->id = identificador;

    // Creación de los "subárboles" izquierdo y derecho.
    temp->izq = NULL;
    temp->der = NULL;
    temp->factor_E = 0;

    return temp;
}


// Rotación de las ramas derechas.
void Arbol::rotacion_DD(Nodo *&raiz, bool altura, Nodo *nodo1, int tipo_rotacion){

    raiz->der = nodo1->izq;
    nodo1->izq = raiz;

    // Condicionales utilizadas para determinar si el procedimiento a llevar
    // es el de reestructuración o el de agregar, debido a que estos poseen una
    // leve diferencia en su código.
    // Para agregar.
    if(tipo_rotacion == 1){
        raiz->factor_E = 0;
        raiz = nodo1;
    }

    // Para reestructuración por izquierda.
    else if(tipo_rotacion == 2){
        // Se actualiza el Factor de cambio de los nodos que han sido modificados.
        if(nodo1->factor_E == 0){
            raiz->factor_E = 1;
            nodo1->factor_E = -1;
            altura = false;
        }

        else if(nodo1->factor_E == 1){
            raiz->factor_E = 0;
            nodo1->factor_E = 0;
        }

        // Se le asigna un nuevo valor a la raíz.
        raiz = nodo1;
    }
}


// Rotación de las ramas derechas y luego de las izquierdas.
void Arbol::rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2, int tipo_rotacion){

    nodo2 = nodo1->izq;
    raiz->der = nodo2->izq;
    nodo2->izq = raiz;
    nodo1->izq = nodo2->der;
    nodo2->der = nodo1;

    if(nodo2->factor_E == 1){
        raiz->factor_E = -1;
    }

    else{
        raiz->factor_E = 0;
    }

    if(nodo2->factor_E == -1){
        nodo1->factor_E = 1;
    }

    else{
        nodo1->factor_E = 0;
    }

    // Condicionales utilizadas para determinar si el procedimiento a llevar
    // es el de reestructuración o el de agregar, debido a que estos poseen una
    // leve diferencia en su código.
    // Para agregar.
    if(tipo_rotacion == 1){
        raiz = nodo2;
    }
    // Para reestructuracion por izquierda.
    else if(tipo_rotacion == 2){
        raiz = nodo2;
        nodo2->factor_E;
    }
}


// Reestructuración del Árbol por izquierda.
void Arbol::reestructuracion_por_izq(Nodo *&raiz, bool altura){

    Nodo *nodo1;
    Nodo *nodo2;

    if(altura == true){
      // Se verifica el valor del Factor de Equilibrio para poder determinar
      // si se necesita realizar una reestructuración por derecha o no.
        if(raiz->factor_E == -1){
            raiz->factor_E = 0;
        }

        else if(raiz->factor_E == 0){
            raiz->factor_E = 1;
            altura = false;
        }

        else if(raiz->factor_E == 1){
            nodo1 = raiz->der;
            // Se identifica el tipo de rotación que se debe realizar.
            if(nodo1->factor_E >= 0){
                // Rotación de ramas derechas.
                rotacion_DD(raiz, altura, nodo1, 2);
            }

            else{
                // Rotación de ramas derechas y luego de las izquierdas.
                rotacion_DI(raiz, nodo1, nodo2, 2);
            }
        }
    }
}


// Rotación de las ramas izquierdas.
void Arbol::rotacion_II(Nodo *&raiz, bool altura, Nodo *nodo1, int tipo_rotacion){

    // Nodo creado será almacenado por la raíz por derecha.
    raiz->izq = nodo1->der;
    // Nodo por derecha almacenará a la raíz.
    nodo1->der = raiz;

    // Condicionales utilizadas para determinar si el procedimiento a llevar
    // es el de reestructuración o el de agregar, debido a que estos poseen una
    // leve diferencia en su código.
    // Para agregar.
    if(tipo_rotacion == 1){
        raiz->factor_E = 0;
        raiz = nodo1;
    }
    // Para reestructuración por derecha.
    else if(tipo_rotacion == 2){
        // Se actualiza el Factor de cambio de los nodos que han sido modificados.
        if(nodo1->factor_E == 0){
            raiz->factor_E = -1;
            nodo1->factor_E = 1;
            altura = false;
        }

        else if(nodo1->factor_E == -1){
            raiz->factor_E = 0;
            nodo1->factor_E = 0;
        }

        // Se le asigna un nuevo valor a la raíz.
        raiz = nodo1;
    }
}


// Rotación de las ramas izquierdas y luego de las derechas.
void Arbol::rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2, int tipo_rotacion){

    nodo2 = nodo1->der;
    raiz->izq = nodo2->der;
    nodo2->der = raiz;
    nodo1->der = nodo2->izq;
    nodo2->izq = nodo1;

    if(nodo2->factor_E == -1){
        raiz->factor_E = 1;
    }

    else{
        raiz->factor_E = 0;
    }

    if(nodo2->factor_E == 1){
        nodo1->factor_E = -1;
    }

    else{
        nodo1->factor_E = 0;
    }

    // Condicionales utilizadas para determinar si el procedimiento a llevar
    // es el de reestructuración o el de agregar, debido a que estos poseen una
    // leve diferencia en su código.
    // Para agregar.
    if(tipo_rotacion == 1){
        raiz = nodo2;
    }
    // Para reestructuracion por derecha.
    else if(tipo_rotacion == 2){
        raiz = nodo2;
        nodo2->factor_E;
    }
}


// Reestructuración del Árbol por derecha.
void Arbol::reestructuracion_por_der(Nodo *&raiz, bool altura){

    Nodo *nodo1;
    Nodo *nodo2;

    if(altura == true){

        // Se verifica el valor del Factor de Equilibrio para poder determinar
        // si se necesita realizar una reestructuración por derecha o no.
        if(raiz->factor_E == 1){
            raiz->factor_E = 0;
        }

        else if(raiz->factor_E == 0){
            raiz->factor_E = -1;
            altura = false;
        }

        else if(raiz->factor_E == -1){
            // Nodo 1 almacenará el valor de la raíz apuntando por izquierda.
            nodo1 = raiz->izq;
            // Se identifica el tipo de rotación que se debe realizar.
            // Si el nodo 1 apuntando al Factor de Equilibrio es menor
            // o igual que cero.
            if(nodo1->factor_E <= 0){
                // Rotación de ramas izquierdas.
                rotacion_II(raiz, altura, nodo1, 2);
            }
            // Si no es de este modo.
            else{
                // Rotación de ramas izquierdas y luego de las derechas.
                rotacion_ID(raiz, nodo1, nodo2, 2);
            }
        }
    }
}


// Se agregan los nodos en su subárbol correspondiente.
void Arbol::agregar_nodo(Nodo *&raiz, string id_a_agregar, bool &altura){

    Nodo *nodo1;
    Nodo *nodo2;

    string identificador;

    if(raiz != NULL){
        // Para agregar nodos por izquierda.
        if(id_a_agregar < raiz->id){
            // Llamada recursiva para el agregado de nodos por derecha.
            agregar_nodo(raiz->izq, id_a_agregar, altura);

              if(altura == true){
                  // Si la raíz apunta a un Factor de Equilibrio igual a 1.
                  if(raiz->factor_E == 1){
                      // Raíz apuntará a un Factor de Equilibrio igual a 0.
                      raiz->factor_E = 0;
                      // La altura será falsa.
                      altura = false;
                  }

                  // Si la raíz apunta a un Factor de Equilibrio igual a 0.
                  else if(raiz->factor_E == 0){
                      // Raíz apuntará a un Factor de Equilibrio igual a -1.
                      raiz->factor_E = -1;
                  }

                  // Si la raíz apunta a un Factor de Equilibrio igual a -1.
                  else if(raiz->factor_E == -1){
                      // Se guarda el valor de la raíz apuntando por izquierda
                      // en nuestro "nodo1".
                      nodo1 = raiz->izq;

                      // Si el nodo1 apunta a un Factor de Equilibrio menor
                      // o igual a 0.
                      if(nodo1->factor_E <= 0){
                          // Se realiza rotación II (Ramas izquierdas).
                          rotacion_II(raiz, altura, nodo1, 1);
                      }

                      // Si no es así.
                      else{
                          // Se realiza rotación ID (Ramas izquierdas
                          // y luego derechas).
                          rotacion_ID(raiz, nodo1, nodo2, 1);
                      }
                      // Raíz apunta a un Factor de Equilibrio igual a 0.
                      raiz->factor_E = 0;
                      // Altura es falsa.
                      altura = false;
                  }

              }
        }
        // Para agregar nodos por derecha.
        else{
            if(id_a_agregar > raiz->id){
                // Llamada recursiva para el agregado de nodos por derecha.
                agregar_nodo(raiz->der, id_a_agregar, altura);

                if(altura == true){
                    // Si la raíz apunta a un Factor de Equilibrio igual a -1.
                    if(raiz->factor_E == -1){
                        // Raíz apuntará a un Factor de Equilibrio igual a 0.
                        raiz->factor_E = 0;
                        // La altura será falsa.
                        altura = false;
                    }

                    // Si la raíz apunta a un Factor de Equilibrio igual a 0.
                    else if(raiz->factor_E == 0){
                        // Raíz apuntará a un Factor de Equilibrio igual a 1.
                        raiz->factor_E = 1;
                    }

                    // Si la raíz apunta a un Factor de Equilibrio igual a 1.
                    else if(raiz->factor_E == 1){
                        // Se guarda el valor de la raíz apuntando por derecha
                        // en nuestro "nodo1".
                        nodo1 = raiz->der;

                        // Si el nodo1 apunta a un Factor de Equilibrio menor
                        // o igual a 0.
                        if(nodo1->factor_E <= 0){
                            // Se realiza rotación DD (Ramas derechas).
                            rotacion_DD(raiz, altura, nodo1, 1);
                        }

                        // Si no es así.
                        else{
                            // Se realiza rotación DI (Ramas derechas
                            // y luego izquierdas).
                            rotacion_DI(raiz, nodo1, nodo2, 1);
                        }
                        // Raíz apunta a un Factor de Equilibrio igual a 0.
                        raiz->factor_E = 0;
                        // Altura es falsa.
                        altura = false;
                    }
                }
            }

            // Si el nodo ingresado ya ha sido ingresado previamente.
            else{
                // Se indica que el ID ya se encuentra en el Árbol.
                cout << "\nEl ID ingresado: '" << id_a_agregar << "' ya se encuentra en el Árbol" << endl;

                // Se solicita que se ingrese el ID nuevamente;
                cout << "\nPor favor, ingreselo nuevamente: ";
                getline(cin, identificador);
                // Llamado recursivo a la función para que se agregue el ID.
                agregar_nodo(raiz, identificador, altura);
            }
        }
    }

    // Si no existe la raíz, se procede a crearla.
    else{
        // Creación de la raíz (Primer nodo).
        raiz = crear_nodo(id_a_agregar);
        altura = true;
        cout << "\nEl ID ingresado ha sido agregado correctamente..." << endl;
        system("sleep 1.5");
    }
}


// Eliminación de un nodo.
void Arbol::eliminar_nodo(Nodo *&raiz, string id_a_agregar, bool &altura){

    Nodo *temp;
    Nodo *aux1;
    Nodo *aux2;

    // Si la raíz existe.
    if(raiz != NULL){

        // Identificación del subárbol al que pertenece el ID.
        // Pertenece al subárbol izquierdo.
        if(id_a_agregar < raiz->id){
            // Llamada recursiva para eliminación del ID por izquierda.
            eliminar_nodo(raiz->izq, id_a_agregar, altura);
            // Reestruturación por izquierda de los ID's restantes.
            reestructuracion_por_izq(raiz, altura);
        }

        // Pertenece al subárbol derecho.
        else{
            if(id_a_agregar > raiz->id){
            // Llamada recursiva para eliminación del ID por derecha.
            eliminar_nodo(raiz->der, id_a_agregar, altura);
            // Reestruturación por derecha de los ID's restantes.
            reestructuracion_por_der(raiz, altura);
            }

            else{
                temp = raiz;
                altura = true;

                // Si el subárbol derecho está vacío, se asigna el valor de la
                // raíz al subárbol izquierdo.
                if(temp->der == NULL){
                    // Asignación del subárbol izquierdo a raíz.
                    raiz = temp->izq;
                    // Se indica al usuario que el ID ha sido eliminado.
                    cout << "\nEl ID ha sido eliminado correctamente." << endl;
                    system("sleep 1.5");
                }

                else{
                    // Si el subárbol izquierdo está vacío, se asigna el valor
                    // de la raíz al subárbol derecho.
                    if(temp->izq == NULL){
                        // Asignación del subárbol derecho a raíz.
                        raiz = temp->der;
                    }

                    else{
                        // Si los ID's tienen dos descendientes.
                        aux1 = raiz->izq;
                        altura = false;

                        // Se realiza una sustitución por el ID's de más
                        // a la derecha.
                        while(aux1->der != NULL){
                            aux2 = aux1;
                            aux1 = aux1->der;
                            altura = true;
                        }

                        // El ID encontrado pasa a ser la nueva raíz.
                        raiz->id = aux1->id;
                        temp = aux1;

                        if(altura == true){
                            aux2->der = aux1->izq;
                        }

                        else{
                            raiz->izq = aux1->izq;
                        }
                        // Llamado a reestructuración por derecha.
                        reestructuracion_por_der(raiz->izq, altura);
                        // Se indica al usuario que el ID ha sido eliminado.
                        cout << "\nEl ID ha sido eliminado correctamente." << endl;
                        system("sleep 1.5");
                    }
                    //cout << "El ID ha sido eliminado correctamente." << endl;
                    //system("sleep 1.5");
                }
                // Liberación de memoria del nodo.
                free(temp);
            }
        }
    }
    // Sino, se indica que el ID no existe.
    else{
        // Se indica que la información no se encuentra en el Árbol.
        cout << "\nEl ID ingresado: '" << id_a_agregar << "' no existe." << endl;
        // Se le informa al usuario que será redirigido al menú principal.
        cout << "\nSerá redirigido al menú principal..." << endl;
        system("sleep 2");
    }
}


// Función para imprimir el Árbol en inorden.
void Arbol::imprimir_inorden(Nodo *raiz){
    // Si la raíz existe, se procederá a imprimir por izquierda y por derecha.
    if(raiz != NULL){
        // Impresión por izquierda.
        imprimir_inorden(raiz->izq);
        cout << raiz->id << " - ";
        // Impresión por derecha.
        imprimir_inorden(raiz->der);
    }
}


// Función encargada de buscar los nodos ingresados por el usuario.
bool Arbol::busqueda_nodo(Nodo *raiz, string id_a_agregar){

    // Si la raíz ya existe.
    if(raiz != NULL){
        // Si el ID que se quiere agregar es menor a la raíz apuntando
        // al "id".
        if(id_a_agregar < raiz->id){
            // Se buscan los nodos por las ramas izquierdas.
            busqueda_nodo(raiz->izq, id_a_agregar);
        }
        // Si no es así.
        else{
            // Si el ID que se quiere ingresar es mayor a la raíz
            // apuntando al "id".
            if(id_a_agregar > raiz->id){
                // Se buscan los nodos por las ramas derechas.
                busqueda_nodo(raiz->der, id_a_agregar);
            }
            // Si no.
            else{
                // Se indica que el ID se ha encontrado.
                cout << "\nSe ha encontrado el ID ingresado.\n" << endl;
                return true;
            }
        }
    }

    // Si no es así.
    else{
        // Se indica que el ID a buscar, aún no ha sido ingresado.
        cout << "\nEl ID solicitado aún no ha sido ingresado.\n" << endl;
        system("sleep 1.5");
        return false;
    }
}
