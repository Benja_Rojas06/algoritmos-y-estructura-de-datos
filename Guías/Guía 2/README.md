# Propuesta solución Guía 2:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se le entrega un tamaño "m" para un puerto y un
tamaño "n" para la pila a "rellenar", luego a este puerto se le pueden agregar
containers, los cuales, a su vez, tendrán un ID y un nombre asociado a una empresa.
Además, dichos containers se pueden imprimir para su visualización.


## Funcionamiento e Interacción:

- El programa comienza solicitando el tamaño del puerto y la pila a generar. Luego
de esto, prosigue con el despligue de un menú con 4 opciones. Siendo la primera de
ellas la de agregar los containers (La cantidad de estos quedará determinada con
las dimensiones que hayamos entregado anteriormente, además, esto es posible gracias
a la función "Push"); la segunda la de eliminar un container (No funciona); la
tercera la de visualizar el puerto generado (Gracias a la función "Imprimir") y,
por último, una opción para cerrar el programa.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Al querer visualizar la pila mientras esta esté vacía, se cerrará el programa
por un error que no se logró solucionar.
- Al momento de estar todos los elementos y querer agregar otro, no se dirá que
el puerto se encuentra lleno, sino que se reemplazarán los ya existentes por los
nuevos.


## Puntos a considerar:

- Se utilizan clases y pilas como se solicita en el laboratorio.
- Se implementa el uso de arreglos para el desarrollo del mismo.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./Programa".
