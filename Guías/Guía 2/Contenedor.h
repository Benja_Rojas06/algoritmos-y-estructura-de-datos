#include <iostream>

using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
    private:
        string id;
        string nombre_empresa;

    public:
        // Constructor.
        Contenedor();

        // Métodos.
        void datos_contenedor();
        void set_id(string id);
        string get_id();
        string get_nombre_empresa();
};
#endif
