#include <iostream>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        // Se inicializa el tope el 0.
        int tope = 0;
        int tamanio_pila;
        Contenedor *array_pila = NULL;
        // Variable sacada del ejemplo del profesor.
        bool band;

    public:
        // Constructor.
        Pila(int tamanio_pila);

        // Métodos para cada acción realizable.
        void Pila_vacia();
        void Pila_llena();
        void Push();
        //void Pop(int dato);
        void Imprimir_pila();
};
#endif
