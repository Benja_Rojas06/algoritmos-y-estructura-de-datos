#include <iostream>
#include <stdlib.h>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;


// Función para agregar mercadería (En containers) al puerto.
void agregar_mercaderia(int tamanio_puerto, int tamanio_pila, Pila **puerto_talca){

    // Variable creada debido a un error existente en el programa. Sin esta
    // variable, lo que sucede es que no se pregunta por el primer ID.
    // Para ver más claramente lo que ocurre, se recomienda comentar lo referente
    // a esta variable (Línea 15 a 18).
    string ayudin;

    cout << "";
    getline(cin, ayudin);

    // Se agregan los datos de cada container dependiendo del tamaño (Dimensión)
    // que se haya escogido para el puerto.
    for(int i = 0; i < tamanio_puerto; i++){
        puerto_talca[i] = new Pila(tamanio_pila);

        for(int j = 0; j < tamanio_pila; j++){
            puerto_talca[i]->Push();
        }
    }
}


// Función con la cual se imprimen los datos del puerto (Cada container con su
// ID y empresa a la cual pertenece).
void imprimir_puerto(int tamanio_puerto, Pila **puerto_talca){

    for(int i = 0; i < tamanio_puerto; i++){
        puerto_talca[i]->Imprimir_pila();
    }
}


// Función que muestra las opciones que posee el menú.
int opc_menu(){

    int accion;

    // Opciones del menú.
    cout << "Bienvenido al Puerto Talquita (Mejor puerto que uno inexistente ";
    cout << "no hay)." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl << endl;
    cout << "1.- Agregar elemento al puerto." << endl;
    cout << "2.- Eliminar elemento del puerto." << endl;
    cout << "3.- Visualizar el Puerto Talquita." << endl;
    cout << "4.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    cin >> accion;

    return accion;
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(int tamanio_puerto, int tamanio_pila, Pila **puerto_talca){

    int accion;
    int num;

    while (true){

        // Llamado función menú para imprimir pantalla inicial.
        accion = opc_menu();

        // Se agrega una mercadería.
        if (accion == 1){
            system("clear");
            // Llamado función para agregar los datos de los containers.
            agregar_mercaderia(tamanio_puerto, tamanio_pila, puerto_talca);

            // Retorno al menú.
            cout << "Volviendo al menú..." << endl;
            system("sleep 1");
            system("clear");
            menu(tamanio_puerto, tamanio_pila, puerto_talca);
            break;
        }

        // Se elimina una mercadería (No funcional).
        else if(accion == 2){
            system("clear");
            cout << "No disponible..." << endl;

            /*cout << "Indique la mercadería a eliminar: ";
            getline(cin, mercaderia);

            // Se elimina el primer mercadería (Invertido) de la pila. */
            system("sleep 1.5");
            system("clear");
            menu(tamanio_puerto, tamanio_pila, puerto_talca);
            break;
        }

        // Se imprime el Puerto Talquita.
        else if(accion == 3){

            system("clear");
            // Se imprime el Puerto con todos los datos que posee.
            imprimir_puerto(tamanio_puerto, puerto_talca);

            // Se solicita un número para volver al menú principal.
            // Se intentó realizar con un string, pero no funcionó
            cout << "Presione cualquier número para volver al menú: ";
            cin >> num;

            // Retorno al menú principal.
            system("sleep 1");
            system("clear");
            menu(tamanio_puerto, tamanio_pila, puerto_talca);
            break;
        }

        // Cierre del programa.
        else if(accion == 4){
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            opc_menu();
        }
      }
}

int main(){

    int tamanio_pila;
    int tamanio_puerto;

    // Creación de la pila.
    Pila *puerto_talca[tamanio_puerto];

    system("clear");
    cout << "Bienvenido al Puerto Talquita (Mejor puerto que uno inexistente ";
    cout << "no hay)." << endl << endl;
    cout << "Primero debe indicar: " << endl << endl;

    // Se solicitan los tamaños para el puerto y para la pila.
    cout << "El tamaño del Puerto Talquita: ";
    cin >> tamanio_puerto;

    cout << "El tamaño de la pila a crear: ";
    cin >> tamanio_pila;
    system("clear");

    // Llamado función "menu" para que se de "inicio" al programa.
    menu(tamanio_puerto, tamanio_pila, puerto_talca);

    return 0;
}
