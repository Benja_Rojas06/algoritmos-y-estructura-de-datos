#include <iostream>
#include "Contenedor.h"

using namespace std;

// Declaración del constructor por defecto.
Contenedor::Contenedor(){}


// Función encargada de solicitar los datos referentes al contenedor.
void Contenedor::datos_contenedor(){
  
    // Se solicita el "ID" del contenedor.
    cout << "Ingrese el número identificador (ID) del contenedor: ";
    getline(cin, this->id);

    // Se solicita el nombre de la empresa.
    cout << "Ingrese el nombre de la empresa dueña del contenedor: ";
    getline(cin, this->nombre_empresa);
}


// Setters
void Contenedor::set_id(string id){
    this->id = id;
}


// Getters
string Contenedor::get_id(){
    return this->id;
}


string Contenedor::get_nombre_empresa(){
    return this->nombre_empresa;
}
