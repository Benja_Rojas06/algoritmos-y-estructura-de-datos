#include <iostream>
#include "Pila.h"
#include "Contenedor.h"

using namespace std;

// Constructor.
Pila::Pila(int tamanio_pila){
    this->tamanio_pila = tamanio_pila;
    this->array_pila = new Contenedor[this->tamanio_pila];
}


// Función para indicar si la pila está vacía o no.
void Pila::Pila_vacia(){
    if(this->tope == 0){
        // La pila está vacía.
        this->band = true;
    }

    else{
        // La pila no está vacía.
        this->band = false;
    }
}


// Función para indicar si la pila está llena o no.
void Pila::Pila_llena(){
    if(this->tope == this->tamanio_pila){
        // La pila está llena.
        this->band = true;
    }

    else{
        // La pila no está llena.
        this->band = false;
    }
}


// Función para agregar elementos a una pila.
void Pila::Push(){
    // Se verifica si la pila no se encuentra llena.
    Pila_llena();

    if (this->band == true){
        // Si la pila está llena, no se puede agregar un nuevo elemento.
        cout << "Desbordamiento, la pila se encuentra llena." << endl;
        cout << "(No se ha podido agregar el elemento)." << endl;
    }

    else{
        // Cambia el tope puesto que ahora tiene un elemento más.
        this->tope = this->tope + 1;
        // El elemento se agrega (Pila no llena) en el nuevo tope.
        this->array_pila[this->tope - 1].datos_contenedor();
        cout << "\n";
    }
}

/* No funcional.
// Función para eliminar elementos de una pila (No funciona).
void Pila::Pop(int dato){
    // Se verifica que la pila no esté vacía.
    Pila_vacia();

    cout << "Eliminando el primer elemento..." << endl << endl;
    system("sleep 1");

    if (this->band == true){
        // Si la pila está vacía, no se puede eliminar un elemento.
        cout << "Subdesbordamiento, la pila se encuentra vacı́a." << endl;
        cout << "(No se ha eliminado ningún elemento)." << endl;
    }

    else{
        // Cambia el tope puesto que ahora tiene un elemento menos.
        dato = this->array_pila[this->tope];
        // El tope se cambia (Pila no vacía) puesto que se ha
        // eliminado un elemento.
        this->tope = this->tope - 1;
        cout << "El elemento se ha eliminado con éxito." << endl;
    }
}
*/

// Función para imrimir la pila.
void Pila::Imprimir_pila(){

    // Se imprime el último elemento agregado y así sucesivamente
    // hasta el primero (Indicado en el ejemplo del profesor).
    cout << "Información de los containers: " << endl << endl;
    for(int i = this->tope; i >= 1; i--){
        // Impresión elemento por posición.
        cout << " ________________________________________" << endl;
        cout << "|\tID\t|\tNombre Empresa\t|" << endl;
        cout << "     " << array_pila[i-1].get_id() << "\t\t\t" << array_pila[i-1].get_nombre_empresa() << endl;
    }
    cout << "\n";
}
