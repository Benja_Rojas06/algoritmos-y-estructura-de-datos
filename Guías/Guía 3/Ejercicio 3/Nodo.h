#include <iostream>

using namespace std;


// Definición de la estrucutura de "Nodo".
typedef struct _Nodo{

    int num_lista;
    struct _Nodo *nodo_sig = NULL;

} Nodo;
