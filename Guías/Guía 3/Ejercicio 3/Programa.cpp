#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;


// Función con la cual se completa la recta numérica con los sucesores
// de los números ingresados.
void completa_recta_num(Lista *lista){

    int numero;

    // Se apunta al primer número de la lista.
    Nodo *temp = lista->get_primero();
    // Irá iterando para tomar el valor del número sucesor.
    Nodo *temp2;

    temp2 = temp->nodo_sig;

    // Ciclo con el cual se agrega el sucesor del número actual.
    while(temp2 != NULL){

        if((temp2->num_lista) != (temp->num_lista + 1)){
            // Creación del sucesor.
            numero = temp->num_lista + 1;

            // Sucesor se agrega a la lista (recta).
            lista->crear_lista(numero);
        }
        // Se apunta al siguiente número (nodo).
        temp = temp2;
        temp2 = temp2->nodo_sig;
    }
}


// Función con la cual se ingresan los números a través de un ciclo indefinido.
void ingreso_numeros(Lista *lista){

    string num_lista;
    string letra = "N";

    // Variable creada debido a un error existente en el programa. Sin esta
    // variable, lo que sucede es que no se preguntar por los números a ingresar
    // en la lista. Para ver más claramente lo que ocurre, se recomienda comentar
    // lo referente a esta variable (Línea 16 a 19).
    string ayudin;

    cout << "";
    getline(cin, ayudin);

    // Se indica al usuario que cuando presione enter, se dará
    // cierre al programa.
    cout << "\nPara dejar de ingresar números, presione 'Enter'." << endl;

    // Ciclo while se mantendrá hasta que cambie el estado de "letra".
    while(letra != "S"){
        // Se solicitan los números a ingresar en la lista.
        cout << "\nIngrese el número que desea añadir a la lista: ";
        getline(cin, num_lista);

        if(num_lista == ""){
            system("clear");
            cout << "Muchas gracias por agregar números..." << endl;
            // Se cambia el estado de letra, por lo tanto, se cierra el programa.
            letra = "S";
        }

        else{
            // Se agrega el número ingresado a la lista (De manera ordenada).
            lista->crear_lista(stoi(num_lista));
        }
    }
}


// Función que muestra las opciones que posee el menú.
int opc_menu(){

    int accion;

    // Opciones del menú.
    system("clear");
    cout << "\t.~Bienvenido al Completador de Rectas~." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl << endl;
    cout << "1.- Agregar números a la recta (lista)." << endl;
    cout << "2.- Visualizar la recta autocompletada." << endl;
    cout << "3.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    cin >> accion;

    return accion;
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(Lista *lista){

    int accion;
    int eleccion;
    int salir;

    while (true){

        // Llamado función menú para imprimir las opciones del menú.
        accion = opc_menu();

        // Se agregan los números a las listas.
        if (accion == 1){
            system("clear");
            ingreso_numeros(lista);
            completa_recta_num(lista);
            menu(lista);
            break;
        }

        // Se imprimen las 3 listas.
        else if(accion == 2){
            system("clear");
            cout << "    Las recta (lista) autocompletada es: " << endl << endl;

            cout << "[";
            lista->imprimir_lista();
            cout << "]" << endl;

            // Retorno al menú presionando cualquier número.
            cout << "\nPresione cualquier número para volver al menú principal: ";
            cin >> salir;

            // Condicionales para volver al menú.
            if(salir == 0){
                menu(lista);
            }
            else{
                menu(lista);
            }
            break;
        }

        // Cierre del programa.
        else if(accion == 3){
            // Mensaje de despedida.
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            // Se cierra el programa.
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            opc_menu();
        }
    }
}


// Función principal.
int main(){

    // Creación de la lista a utilizar.
    Lista *lista = new Lista();

    // Se envía la lista a la función menú.
    menu(lista);

    return 0;
}
