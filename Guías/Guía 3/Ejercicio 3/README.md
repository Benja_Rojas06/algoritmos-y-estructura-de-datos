# Propuesta solución Guía 3, Ejercicio 3:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se ingresan "n" números que serán agregados a
una lista. Luego, se toma la lista generada (Ej: 1, 3, 9) y se completan los números
que no fueron ingresados a la lista. De este modo, el ejemplo anterior quedaría:
1, 2, 3, 4, 5, 6, 7, 8, 9.
Cabe mencionar que los números serán agregados hasta que el usario se canse de hacerlo.


## Funcionamiento e Interacción:

- El programa comienza mostrando el menú del mismo en el que se pueden realizar
3 acciones. La primera consiste en agregar los números a la lista, estos números
serán agregados hasta que el usuario presione "Enter", al hacerlo, se le redigirá
al menú principal. La segunda se encarga de imprimir la lista autocompletada, lo
cual se realiza gracias a la función "completa_recta_num" que completa la lista o
recta númerica con los números faltantes y, posterior a ello, la procede a imprimir.
La tercera acción correspende al cierre del programa.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Al ingresar conjuntos de números "descendentes" por segunda vez, el código se
corrompe al intentar ingresar ingresarlos, puesto que los antiguos no se
borrar, se juntan con los nuevos y aparecen números extras.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de listas enlazadas simples para el desarrollo del mismo.
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa".
