#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{

    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        // Constructor.
        Lista();

        // Métodos definidos para crear, ordenar e imprimir la lista.
        void crear_lista(int num_lista);
        void ordenar_lista(Nodo *temp);
        void imprimir_lista();
        Nodo* get_primero();
};
#endif
