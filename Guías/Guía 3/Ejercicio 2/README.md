# Propuesta solución Guía 3, Ejercicio 2:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se ingresan "n" números que serán agregados a
una lista, lista 1 o lista 2 dependiendo de lo que indique el usuario. Luego, se
toma la lista (1 o 2) generada (Ej: 1, 2, 3) y se enlaza con una tercera lista;
de este modo, tenemos dos listas iguales. Ahora bien, al momento de seleccionar
la otra lista para ingresar los valores, esta también se enlazará con la lista 3
y, de este modo, tendremos una lista 3 que contiene los números que se ingresaron
tanto a la lista 1 como a la lista 2.
- Los números serán agregados hasta que el usario se canse de hacerlo.


## Funcionamiento e Interacción:

- El programa comienza mostrando el menú del mismo en el que se pueden realizar
3 acciones. La primera consiste en agregar los números a la lista, luego se mostrarán
dos opciones en pantalla, la primera para agregar los números a la lista 1 y la
segunda para agregar los números a la lista 2, estos números serán agregados hasta
que el usuario presione "Enter", al hacerlo, se le redigirá al menú principal,
después de agregar los númreos a cualquiera de las dos listas, se rederigirá al
menú principal. La segunda se encarga de imprimir la lista 1, 2 y 3, siendo las
primeras dos las ingresadas por el usuario y la tercera la enlazada resultante de
las dos anteriores. La tercera acción correspende al cierre del programa.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Al ingresar conjuntos de números "descendentes" por segunda vez, el código se
corrompe al intentar ingresar ingresarlos, puesto que los antiguos no se
borrar, se juntan con los nuevos y aparecen números extras.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de listas enlazadas simples para el desarrollo del mismo.
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa".
