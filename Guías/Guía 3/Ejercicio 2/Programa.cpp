#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;


// Función con la cual se ingresan los números a través de un ciclo indefinido.
void ingreso_numeros(Lista *lista){

    string num_lista;
    string letra = "N";

    // Variable creada debido a un error existente en el programa. Sin esta
    // variable, lo que sucede es que no se preguntar por los números a ingresar
    // en la lista. Para ver más claramente lo que ocurre, se recomienda comentar
    // lo referente a esta variable (Línea 16 a 19).
    string ayudin;

    cout << "";
    getline(cin, ayudin);

    // Se indica al usuario que cuando presione enter, se dará
    // cierre al programa.
    cout << "\nPara dejar de ingresar números, presione 'Enter'." << endl;

    // Ciclo while se mantendrá hasta que cambie el estado de "letra".
    while(letra != "S"){
        // Se solicitan los números a ingresar en la lista.
        cout << "\nIngrese el número que desea añadir a la lista: ";
        getline(cin, num_lista);

        if(num_lista == ""){
            system("clear");
            cout << "Muchas gracias por agregar números..." << endl;
            // Se cambia el estado de letra, por lo tanto, se cierra el programa.
            letra = "S";
        }

        else{
            // Se agrega el número ingresado a la lista (De manera ordenada).
            lista->crear_lista(stoi(num_lista));
        }
    }
}


// Función encargada de enlazar la lista 1 y 2 con la 3.
void enlazar_listas(Lista *lista, Lista *lista3){

    int numero;
    // Utilziación puntero que, valiendo la redundancia, apunta al
    // primer número de la lista.
    Nodo *temp = lista->get_primero();

    // Ciclo con el cúal se enlazarán los números de las listas 1 y 2
    // a la lista 3 de manera ordenada.
    while(temp != NULL){
        // Se agrega el número de la lista 1 o 2 a la lista 3.
        numero = temp->num_lista;

        // Se crea la lista 3 con los números ordenados.
        lista3->crear_lista(numero);

        // Se apunta al siguiente número (nodo).
        temp = temp->nodo_sig;
    }
}


// Función que muestra las opciones que posee el menú.
int opc_menu(){

    int accion;

    // Opciones del menú.
    system("clear");
    cout << "\t.~Bienvenido al Enlazador de Listas~." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl << endl;
    cout << "1.- Agregar números a una lista." << endl;
    cout << "2.- Visualizar las listas ingresadas y la enlazada." << endl;
    cout << "3.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    cin >> accion;

    return accion;
}


// Función menú con la cual se llevan a cabo todas las opciones de la función
// opc_menu.
int menu(Lista *lista1, Lista *lista2, Lista *lista3){

    int accion;
    int eleccion;
    int salir;

    while (true){

        // Llamado función menú para imprimir las opciones del menú.
        accion = opc_menu();

        // Se agregan los números a las listas.
        if (accion == 1){
            system("clear");
            // Se solicita que se elija una de las 2 listas
            // para ingresar los números.
            cout << "\tIndique a lista desea agregar los números." << endl << endl;
            cout << "1.- Agregar númeos a la lista 1." << endl;
            cout << "2.- Agregar números a la lista 2." << endl << endl;

            cout << "Ingrese la lista: ";
            cin >> eleccion;

            // Se ingresan los números a la lista 1 y, posteriormente se enlaza
            // esta con la lista 3.
            if(eleccion == 1){
                // Ingreso lista 1.
                ingreso_numeros(lista1);
                // Enlazamiento con lista 3.
                enlazar_listas(lista1, lista3);
            }

            // Se ingresan los números a la lista 2 y, posteriormente se enlaza
            // esta con la lista 3.
            else if(eleccion == 2){
                // Ingreso lista 2.
                ingreso_numeros(lista2);
                // Enlazamiento con lista 3.
                enlazar_listas(lista2, lista3);
            }

            // Cualquier otra cosa ingresada es tomada como inválida y, en
            // "penalización" se envía al usuario al menú principal.
            else{
                cout << "\nOpción inválida." << endl;
                cout << "Se le redigirá al menú principal." << endl;
                system("sleep 2");
            }

            menu(lista1, lista2, lista3);
            break;
        }

        // Se imprimen las 3 listas.
        else if(accion == 2){
            system("clear");
            cout << "    Las listas ingresadas y la enlazada son: " << endl << endl;

            // Impresión lista 1.
            cout << "\tLista 1:" << endl;
            cout << "[";
            lista1->imprimir_lista();
            cout << "]" << endl;

            // Impresión lista 2.
            cout << "\n\tLista 2:" << endl;
            cout << "[";
            lista2->imprimir_lista();
            cout << "]" << endl;

            // Impresión lista 3.
            cout << "\n\tLista 3 (Enlazada):" << endl;
            cout << "[";
            lista3->imprimir_lista();
            cout << "]" << endl;

            // Retorno al menú presionando cualquier número.
            cout << "\nPresione cualquier número para volver al menú principal: ";
            cin >> salir;

            // Condicionales para volver al menú.
            if(salir == 0){
                menu(lista1, lista2, lista3);
            }
            else{
                menu(lista1, lista2, lista3);
            }
            break;

        }

        // Cierre del programa.
        else if(accion == 3){
            // Mensaje de despedida.
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            // Se cierra el programa.
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            opc_menu();
        }
    }
}


// Función principal.
int main(){

    // Creación de las 3 listas a utilizar.
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();

    // Se envían las listas a la función menú.
    menu(lista1, lista2, lista3);

    return 0;
}
