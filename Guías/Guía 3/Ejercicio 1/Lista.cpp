#include <iostream>
#include "Lista.h"

using namespace std;


// Constructor por defecto.
Lista::Lista(){}

// Función que se encarga de la creación de la lista y, además,
// del enlazamiento de los nodos.
void Lista::crear_lista(int num_lista){

    // Creación de un nodo.
    Nodo *temp = new Nodo;

    // Se asigna la instancia a "num_lista".
    temp-> num_lista = num_lista;

    // Se apunta a NULL por defecto.
    temp->nodo_sig = NULL;

    // Al agregarse el primer nodo a la lista, este quedará como primero y
    // como último al mismo tiempo.
    if(this->primero == NULL){
        this->primero = temp;
        this->ultimo = this->primero;
    }

    // Si ya existe un nodo, se define al agregado como el último.
    else{
        this->ultimo->nodo_sig = temp;
        this->ultimo = temp;
    }

    ordenar_lista(this->primero);
}


// Función con la cual se procede a ordenar los números ingresados a la lista.
void Lista::ordenar_lista(Nodo *temp){

    int aux;

    // Se utiliza el método burbuja para ordenar la lista.
    while(temp != NULL){
        Nodo *temp2 = temp->nodo_sig;
        while(temp2 != NULL){
            if((temp->num_lista) > (temp2->num_lista)){
                aux = temp2->num_lista;
                temp2->num_lista = temp->num_lista;
                temp->num_lista = aux;
            }
            temp2 = temp2->nodo_sig;
        }
        temp = temp->nodo_sig;
    }
}


// Función con la cual se imprime la lista con cada número agregado.
void Lista::imprimir_lista(){
    // Creación de una variable temporal "temp" con la cual se
    // recorrerá la lista.
    Nodo *temp = this->primero;

    // Se recorre la lista hasta que no existan más nodos.
    while(temp != NULL){
        cout << "  " << temp->num_lista << "  ";
        temp = temp->nodo_sig;
    }
}
