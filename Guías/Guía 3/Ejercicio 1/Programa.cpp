#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;


// Función principal.
int main(){

    Lista *lista = new Lista();
    string num_lista;
    string letra = "N";

    system("clear");
    cout << "\tOrdenar tus números nunca fue tan fácil" << endl << endl;

    // Se indica al usuario que cuando presione enter, se dará
    // cierre al programa.
    cout << "Para dejar de ingresar números, presione 'Enter'." << endl;

    // Ciclo while se mantendrá hasta que cambie el estado de "letra".
    while(letra != "S"){
        // Se solicitan los números a ingresar en la lista.
        cout << "\nIngrese el número que desea añadir a la lista: ";
        getline(cin, num_lista);

        if(num_lista == ""){
            system("clear");
            cout << "Muchas gracias por agregar números..." << endl;
            // Se cambia el estado de letra, por lo tanto, se cierra el programa.
            letra = "S";
        }

        else{
            // Se agrega el número ingresado a la lista (De manera ordenada).
            lista->crear_lista(stoi(num_lista));

            // Se procede a mostrar el estado de la lista por cada ingreso.
            cout << "\nEstado actual de la lista: " << endl;
            lista->imprimir_lista();
            cout << "" << endl;
        }
    }
    return 0;
}
