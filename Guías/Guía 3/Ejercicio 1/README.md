# Propuesta solución Guía 3, Ejercicio 1:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se ingresan "n" números que serán agregados a
una lista. Dicha lista se irá imprimiendo con cada número agregado para actualizar
el estado de esta.
- Los números serán agregados hasta que el usario se canse de hacerlo.


## Funcionamiento e Interacción:

- El programa comienza mostrando la pantalla incial en la que se le indica al
usuario que puede ingresar números hasta que presione "Enter". Cuando el usuario
ingrese un número entero, se procederá a imprimir la lista generada hasta el momento,
además, dicha lista estará ordenada de menor a mayor (Gracias al método burbuja).
Cuando el usuario presione "Enter", se dará por finalizado el programa, limpiando
la pantalla e imprimiendo un mensaje de despedida.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.


## Construcción del programa:

- Sistema operativo: Ubuntu 18.04.4 LTS.
- Atom: Editor de texto utilizado para escribir el código del programa.
- C++: Lenguaje utilizado para resolver la problemática.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de listas enlazadas simples para el desarrollo del mismo.
- Para la compilación del programa se utiliza el comando "make" en la terminal y,
posterior a ello, se ejecuta con el comando "./Programa".
