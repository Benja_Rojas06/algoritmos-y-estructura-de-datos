#include <iostream>
#include "Pila.h"

using namespace std;

// Constructor.
Pila::Pila(int tamanio){
    this->tamanio = tamanio;
    this->array_pila = new int[this->tamanio];
}


// Función para indicar si la pila está vacía o no.
void Pila::Pila_vacia(){
    if(this->tope == 0){
        // La pila está vacía.
        this->band = true;
    }

    else{
        // La pila no está vacía.
        this->band = false;
    }
}


// Función para indicar si la pila está llena o no.
void Pila::Pila_llena(){
    if(this->tope == this->tamanio){
        // La pila está llena.
        this->band = true;
    }

    else{
        // La pila no está llena.
        this->band = false;
    }
}


// Función para agregar elementos a una pila.
void Pila::Push(int dato){
    // Se verifica si la pila no se encuentra llena.
    Pila_llena();

    cout << "Agregando nuevo elemento..." << endl << endl;
    system("sleep 1");

    if (this->band == true){
        // Si la pila está llena, no se puede agregar un nuevo elemento.
        cout << "Desbordamiento, la pila se encuentra llena." << endl;
        cout << "(No se ha podido agregar el elemento)." << endl;
    }

    else{
        // Cambia el tope puesto que ahora tiene un elemento más.
        this->tope = this->tope + 1;
        // El elemento se agrega (Pila no llena) en el nuevo tope.
        this->array_pila[this->tope] = dato;
        cout << "El elemento se ha agregado con éxito." << endl;
    }
}


// Función para eliminar elementos de una pila.
void Pila::Pop(int dato){
    // Se verifica que la pila no esté vacía.
    Pila_vacia();

    cout << "Eliminando el primer elemento..." << endl << endl;
    system("sleep 1");

    if (this->band == true){
        // Si la pila está vacía, no se puede eliminar un elemento.
        cout << "Subdesbordamiento, la pila se encuentra vacı́a." << endl;
        cout << "(No se ha eliminado ningún elemento)." << endl;
    }

    else{
        // Cambia el tope puesto que ahora tiene un elemento menos.
        dato = this->array_pila[this->tope];
        // El tope se cambia (Pila no vacía) puesto que se ha
        // eliminado un elemento.
        this->tope = this->tope - 1;
        cout << "El elemento se ha eliminado con éxito." << endl;
    }
}


// Función para imrimir la pila.
void Pila::Imprimir_pila(){
    // Se verifica que la pila no esté vacía.
    Pila_vacia();

    cout << "Adquiriendo elementos de la pila..." << endl << endl;
    system("sleep 1");

    if (this->band == true){
        // Si la pila está vacía, no se puede imprimir la pila.
        cout << "Aún no se ha agregado ningún elemento a la pila." << endl;
        cout << "(Su pila se encuentra vacía)." << endl;
    }

    else{
        cout << "La pila que usted ha generado es: " << endl << endl;
        // Se imprime el último elemento agregado y así sucesivamente
        // hasta el primero (Indicado en el ejemplo del profesor).
        for(int i = this->tope; i >= 1; i--){
            // Impresión elemento por posición.
            cout << "\t     |" << array_pila[i] << "|" << endl;
        }
    }
}
