#include <iostream>
#include "Pila.h"

using namespace std;

int menu(){

    string accion;

    // Opciones del menú.
    cout << "Bienvenido a PILA." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl << endl;
    cout << "1.- Agregar elemento a una pila." << endl;
    cout << "2.- Eliminar elemento de la pila." << endl;
    cout << "3.- Visualizar la pila." << endl;
    cout << "4.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    getline(cin, accion);

    return stoi(accion);
}

int main(){

    int accion;
    string tamanio;
    string elemento;

    system("clear");
    cout << "Bienvenido a PILA." << endl << endl;
    cout << "Primero debe indicar el tamaño de la pila a crear: ";
    getline(cin, tamanio);
    system("clear");

    // Creación de la pila.
    Pila pila = Pila(stoi(tamanio));

    while (true){

        // Llamado función menú para imprimir pantalla inicial.
        accion = menu();

        // Se agrega un elemento.
        if (accion == 1){

            system("clear");
            cout << "Indique el elemento que desea agregar: ";
            getline(cin, elemento);

            // Se agrega el elemento a la pila.
            pila.Push(stoi(elemento));
            system("sleep 1.8");
            system("clear");
            continue;
        }

        // Se elimina un elemento.
        else if(accion == 2){
            system("clear");
            cout << "Presione cualquier número eliminar el primer elemento: ";
            getline(cin, elemento);

            // Se elimina el primer elemento (Invertido) de la pila.
            pila.Pop(stoi(elemento));
            system("sleep 1.8");
            system("clear");
            continue;
        }

        // Se imprime la pila generada hasta el momento.
        else if(accion == 3){
            system("clear");
            // Se imprime la pila.
            pila.Imprimir_pila();
            system("sleep 2");
            system("clear");
            continue;
        }

        // Cierre del programa.
        else if(accion == 4){
            system("clear");
            cout << "Que tenga un buen día..." << endl;
            system("sleep 1");
            exit(0);
            break;
        }

        // "Reinicio" del programa por una acción inválida.
        else{
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            menu();
        }
      }
    return 0;
}
