#include <iostream>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        // Se inicializa el tope el 0.
        int tope = 0;
        int tamanio;
        int *array_pila = NULL;
        // Variables sacadas del ejemplo del profesor.
        int dato;
        bool band;

    public:
        // Constructor.
        Pila(int tamanio);

        // Métodos para cada acción realizable.
        void Pila_vacia();
        void Pila_llena();
        void Push(int dato);
        void Pop(int dato);
        void Imprimir_pila();
};
#endif
