# Propuesta solución Laboratorio 2:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se le entrega un tamaño "n" para la pila a
"rellenar", luego a esta pila se le pueden agregar y eliminar elementos (Según
lo decida el usuario) y, además, se puede imprimir la pila generada.


## Funcionamiento e Interacción:

- El programa comienza solicitando el tamaño de la pila a generar. Luego de
esto, prosigue con el despligue de un menú con 4 opciones. Siendo la primera de
ellas la de agregar un elemento (Gracias a la función "Push") y si queremos
agregar un 4to eleemnto siendo que designamos el tamaño de la pila como "3", se
le avisará de este error al usuario y se le redigirá al menú principal; la
segunda la de eliminar el primer elemento de la pila (Gracias a la función
"Pop") y si deseamos eliminar un elemento cuando la pila esté vacía, se le
notificará de este error al usuario y se le redigirá al menú principal; la
tercera la de visualizar la pila generada hasta el momento (Gracias a la función
"Imprimir") y,por último, una opción para cerrar el programa.
Cabe mencionar que los elementos se agregan por separado, es decir, se debe
seleccionar la acción "1", agregar un elemento, luego se vuelve al menú
principal y, nuevamente, seleccionar la acción "1" para agregar otro elemento si
así lo desea el usuario. Lo anterior con el fin de ver las modificaciones hechas
a la pila por cada elemento que se le agrega.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.


## Puntos a considerar:

- Se utilizan clases y pilas como se solicita en el laboratorio.
- Se implementa el uso de arreglos para el desarrollo del mismo.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./Programa".
