#include <iostream>

using namespace std;

#ifndef CUADRADOS_H
#define CUADRADOS_H

class Cuadrados{

    public:
        Cuadrados();
        int suma_cuadrados(int *numeros, int cant_num);
};
#endif
