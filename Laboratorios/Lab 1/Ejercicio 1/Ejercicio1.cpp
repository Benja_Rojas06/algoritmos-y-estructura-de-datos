#include <iostream>
#include <stdlib.h>
#include "Cuadrados.h"

using namespace std;

int main(){

    int cant_num;
    Cuadrados cuadrado = Cuadrados();

    system("clear");
    cout << "Indique la cantidad de números a ingresar: ";
    cin >> cant_num;

    // Creación de arreglo de "n" elementos.
    int numeros[cant_num];

    // LLenado, por posición (Estas parten del 1), del arreglo creado.
    for(int i = 1; i <= cant_num; i++){
        cout << "Indique el número que va en la posición " << i << " del arreglo: ";
        cin >> numeros[i];
    }
    // Arreglo se envía a la clase "Cuadrado" para que esta retorne el
    // valor de la suma de los cuadrados los números del arreglo.
    cout << "\nLa suma del cuadrado de los números que ha ";
    cout << "ingresado es: " << cuadrado.suma_cuadrados(numeros, cant_num) << endl;
}
