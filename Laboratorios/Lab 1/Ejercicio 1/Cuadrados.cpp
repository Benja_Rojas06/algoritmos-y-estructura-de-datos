#include <iostream>
#include "Cuadrados.h"

using namespace std;

// Constructor por defecto.
Cuadrados::Cuadrados(){}


int Cuadrados::suma_cuadrados(int *numeros, int cant_num){

    // Se inicializa la suma desde 0.
    int suma = 0;

    // A la variable suma se le irá sumando el cuadrado del número anterior
    // hasta el último número ingresado por el usuario.
    for(int i = 1; i <= cant_num; i++){
        suma = suma + (numeros[i] * numeros[i]);
    }
    // Retorno suma de todos los cuadrados.
    return suma;
}
