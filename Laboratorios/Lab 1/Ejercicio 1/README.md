# Propuesta solución Ejercicio 1, Laboratorio 1:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se le entrega una cantidad "n" de números, luego
estos se elevan al cuadrado y, por último, se suman.


## Funcionamiento e Interacción:

- El programa comienza solicitando al usuario la cantidad de números que quiere
ingresar. Posterior a ello, deberá ingresar tantos números como la cantidad de
números indicados anteriormente y, para finalizar, el programa se encargará de
elevar cada número al cuadrado y sumar estos cuadrados con la clase "Cuadrados".


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de arreglos para el desarrollo del mismo.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./Ejercicio1".
