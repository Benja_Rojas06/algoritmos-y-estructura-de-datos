# Propuesta solución Ejercicio 3, Laboratorio 1:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se le entrega una cantidad "n" de clientes,
luego a cada uno de estos clientes se le ingresa un nombre, teléfono, saldo y
estado (moroso o no). Al finalizar, el ingreso de los datos solicitados, se da
paso a la impresión de los datos de cada cliente.


## Funcionamiento e Interacción:

- El programa comienza con el despliegue de un menú en el que se pueden visualizar
dos opciones: Ingresar cliente o salir. Si seleccionamos la primera, se solicitará
al usuario la cantidad de clientes que quiere ingresar, posterior a ello, deberán
ingresar los datos correspondientes a cada cliente (nombre, teléfono, saldo y estado)
ingresado y estos serán asignados a valores gracias a la clase "Cliente". Para
finalizar, el programa se encargará de imprimir los datos entregados en un inicio.


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en cualquier entrada que solicite uno
de tipo "int", el programa se rompe automáticamente y se da cierre al mismo.
- Si se ingresa una combinación de letras y números en la frase solicitada, el
programa se rompe.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de arreglos para el desarrollo del mismo.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./Ejercicio3".
