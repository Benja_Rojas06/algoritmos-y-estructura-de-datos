#include <iostream>
#include <stdlib.h>
#include "Cliente.h"

using namespace std;

int menu(){

    string accion;
    system("clear");

    cout << "Bienvenido al Registrador de Clientes." << endl << endl;
    cout << "¿Que acción desea realizar?" << endl << endl;
    cout << "1.- Ingresar datos de un cliente." << endl;
    cout << "2.- Salir." << endl;

    cout << "\nIngrese la acción que desea realizar: ";
    getline(cin, accion);

    return stoi(accion);
}

void imprimir_datos_clientes(Cliente *clientes, int cant_clientes){

    system("clear");
    cout << "Los clientes que han sido ingresados son: " << endl;

    // Se recorre el arreglo "clientes" para imprimir cada cliente, sus datos
    // y su estado (moroso o no).
    for(int i = 0; i < cant_clientes; i++){
        cout << "" << endl;
        cout << (i + 1) << ".- Cliente: " << clientes[i].get_nombre() << endl;
        cout << "    Teléfono: " << clientes[i].get_telefono() << endl;
        cout << "    Saldo: $" << clientes[i].get_saldo() << endl;

        if(clientes[i].get_moroso()){
            cout << "    CUIDADO, ESTE CLIENTE ES MOROSO" << endl;
        }
        else{
          cout << "    Este cliente no es moroso" << endl;
        }
    }
}

void datos_clientes(Cliente *clientes, int cant_clientes){

    string nombre;
    string telefono;
    string saldo;
    string moroso;

    system("clear");
    cout << "A continuación ingrese todos los datos de los clientes." << endl;

    // Se repite hasta que se ingresen la totalidad de clientes.
    for(int i = 0; i < cant_clientes; i++){
        cout << "\nCliente número " << (i + 1) << " a ingresar: " << endl;

        // Se solicitan cada uno de los datos que posee el cliente.
        cout << "Nombre: ";
        getline(cin, nombre);

        cout << "Teléfono: ";
        getline(cin, telefono);

        cout << "Saldo: ";
        getline(cin, saldo);

        cout << "¿El cliente es moroso?" << endl;
        cout << "    S: Sí es moroso." << endl;
        cout << "    Cualquier otra tecla para indicar que no lo es." << endl;
        cout << "    Su respuesta: ";
        getline(cin, moroso);

        // Se guardan los datos ingresados.
        clientes[i].set_nombre(nombre);
        clientes[i].set_telefono(telefono);
        clientes[i].set_saldo(stoi(saldo));

        // Dependiendo de si el cliente es moroso o no, se le asigna un
        // valor en tipo "bool".
        if(moroso == "S"){
            // True.
            clientes[i].set_moroso(1);
        }
        else{
            // False.
            clientes[i].set_moroso(0);
        }
    }
}

int main(){

    int accion;
    string cant_clientes;

    while (true){

        // Llamado función menú para imprimir pantalla inicial.
        accion = menu();

        if (accion == 1){

            system("clear");
            cout << "Indique la cantidad de clientes a ingresar: ";
            getline(cin, cant_clientes);

            // Creación del arreglo de objetos de "n" clientes.
            Cliente clientes[stoi(cant_clientes)];

            datos_clientes(clientes, stoi(cant_clientes));
            imprimir_datos_clientes(clientes, stoi(cant_clientes));
            break;
        }

        else if(accion == 2){
              // Cierre del programa.
              system("clear");
              cout << "Que tenga un buen día..." << endl;
              system("sleep 1.5");
              exit(0);
              break;
        }

        else{
            // "Reinicio" del programa por una acción inválida.
            system("clear");
            cout << "Favor, ingrese una acción válida." << endl;
            system("sleep 1.5");
            main();
        }
      }
    return 0;
}
