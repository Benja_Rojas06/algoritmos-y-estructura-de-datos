#include <iostream>

using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H


class Cliente{

    private:
        string nombre;
        string telefono;
        int saldo;
        bool moroso;

    public:
        // Constructor.
        Cliente();

        // Métodos.
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();
};
#endif
