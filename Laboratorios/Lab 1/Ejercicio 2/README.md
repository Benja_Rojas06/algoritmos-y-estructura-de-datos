# Propuesta solución Ejercicio 2, Laboratorio 1:

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Algoritmos y Estructura de Datos.


## Acerca del programa:

- Consiste en un programa al cual se le entrega una cantidad "n" de frases, luego
estas se recorren y se cuentan la cantidad de letras mayúsculas y minúsculas que
posee cada una.


## Funcionamiento e Interacción:

- El programa comienza solicitando al usuario la cantidad de frases que quiere
ingresar. Posterior a ello, deberá ingresar tantas frases como la cantidad de
frases indicadas anteriormente y, para finalizar, el programa se encargará de
contar cada letra minúscula y mayúscula que tiene la frase con la clase "Lector".


## Fallos que posee el programa:

- Al ingresar un caracter de tipo "string" en la primera entrada, el programa se
rompe automáticamente y se da cierre al mismo.
- Si se ingresa una combinación de letras y números en la frase solicitada, el
programa se rompe.


## Puntos a considerar:

- Se utilizan clases como se solicita en el laboratorio.
- Se implementa el uso de arreglos para el desarrollo del mismo.
- El programa se compila gracias al archivo make y luego se ejecuta con
"./Ejercicio2".
