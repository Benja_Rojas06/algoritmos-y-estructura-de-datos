#include <iostream>
#include <ctype.h>
#include "Lector.h"

using namespace std;

// Constructor.
Lector::Lector(){}

// Función con la cual se recorrerán las frases y se retornará
// la cantidad de letras mayúsculas que cada una posea.
int Lector::lector_cant_may(string frases){

    this->cant_may = 0;

    // Se recorre cada una de las frases.
    for(int i = 0; i < frases.size(); i++){
        // Función recomendada por el profesor para contar cantidad
        // de letras mayúsculas.
        if(isupper(frases[i])){
            this->cant_may++;
        }
    }
    // Retorno cantidad de letras mayúsculas.
    return this->cant_may;
}

// Función con la cual se recorrerán las frases y se retornará
// la cantidad de letras minúsculas que cada una posea.
int Lector::lector_cant_min(string frases){

    this->cant_min = 0;

    // Se recorre cada una de las frases.
    for(int i = 0; i < frases.size(); i++){
        // Función recomendada por el profesor para contar cantidad
        // de letras minúsculas.
        if(islower(frases[i])){
            this->cant_min++;
        }
    }
    // Retorno cantidad de letras minúsculas.
    return this->cant_min;
}
