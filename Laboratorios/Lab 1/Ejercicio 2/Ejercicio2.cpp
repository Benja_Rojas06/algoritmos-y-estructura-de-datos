#include <iostream>
#include <stdlib.h>
#include "Lector.h"

using namespace std;

// Imprime la cantidad de letras mayúsculas y minúsculas que poseen las frases.
void imprime_may_min(string *frases, int cant_frases){
    Lector leer = Lector();

    // Impresión de cada una de las frases y su cantidad correspondiente.
    for(int i = 0; i < cant_frases; i++){
        cout << "" << endl;
        cout << (i + 1) << ".- La frase ingresada: " << frases[i] << endl;
        cout << "    Contiene:" << endl;
        cout << "      ~" << leer.lector_cant_may(frases[i]) << " letra(s) mayúsculas." << endl;
        cout << "      ~" << leer.lector_cant_min(frases[i]) << " letra(s) minúsculas." << endl;
    }
}

int main(){

    string cant_frases;

    system("clear");
    cout << "Indique la cantidad de frases a ingresar: ";
    getline(cin, cant_frases);

    // Creación de arreglo de "n" elementos (Tipo "string").
    string frases[stoi(cant_frases)];

    // LLenado, por posición (Estas parten del 1), del arreglo creado.
    for(int i = 0; i < stoi(cant_frases); i++){
        cout << "Indique la frase que va en la posición " << (i + 1) << " del arreglo: ";
        getline(cin, frases[i]);
    }

    imprime_may_min(frases, stoi(cant_frases));

    return 0;
}
