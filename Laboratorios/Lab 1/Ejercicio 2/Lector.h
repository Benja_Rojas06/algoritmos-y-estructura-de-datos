#include <iostream>

using namespace std;

#ifndef LECTOR_H
#define LECTOR_H

class Lector{

    private:
        int cant_may;
        int cant_min;
    public:
        // Constructor.
        Lector();

        // Métodos.
        int lector_cant_may(string frases);
        int lector_cant_min(string frases);
};
#endif
